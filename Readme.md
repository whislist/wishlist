---
# Membre du projet
- LAUNOIS Rémy 
- PALAU Clément 
- NOIROT Quentin 
- COSTANTINI Ugo

---

#Projet Wishlist - S3A

- Lien pour tester le projet :
https://webetu.iutnc.univ-lorraine.fr/www/noirot9u/

- Lien vers le dépôt git du projet :
https://bitbucket.org/whislist/wishlist/

- Lien vers le tableau de bord du projet :
https://trello.com/b/SyrMk6LG/s3awishlist

---

#Indications sur l'installation

Pour installer le projet sur une machine quelconque il faut procéder aux étapes suivantes :

- Cloner le projet grâce au lien git ci-dessus sur sa machine personnel.
- Utiliser les fichiers  sql pour importer les tables et données dans une base de données MySQL, à l’aide de phpmyadmin par exemple.
- Configurer le fichier conf.ini, se trouvant sur le chemins suivant: wishlist\src\conf\conf.ini, qui contient des informations sur la base de données.
- Un exemple de fichier conf.ini :
    - user='root'
    - password=1234
    -  database=’mywishlist’
    - host='localhost'
    - driver='mysql'
    - charset=’utf8’
    - collation=’utf8_unicode_ci’
    - unix_socket =
- Puis mettre à jour le composer.json

---

#Fonctionnalités

Nous avons réalisé toutes les fonctionnalités de niveau 1, de niveau 2 et certaines extensions. Voici la liste des fonctionnalités réalisées de niveau 1 et 2 :

1. Afficher une liste de souhaits
2. Afficher un item d’une liste
3. Réserver un item
4. Ajouter un message avec sa réservation
5. Afficher un message sur une liste
6. Créer une liste
7. Modifier les informations générales d’une de ses listes
8. Ajouter des items
9. Modifier un item
10. Supprimer un item
11. Rajouter une image à un item
12. Modifier une image d’un item
13. Supprimer une image d’un item
14. Partager une liste
16. Consulter les réservations d'une de ses listes avant échéance
17. Consulter les réservations et messages d'une de ses listes après échéance


De plus, les extensions suivantes ont étées réalisées :

1. Créer un compte
2. S’authentifier
3. Modifier son compte
4. Rendre une liste publique
5. Afficher les listes de souhaits publiques
6. Créer une cagnotte sur un item
7. Participer à une cagnotte
8. Uploader une image
