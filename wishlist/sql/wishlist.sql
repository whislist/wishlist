-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 16 jan. 2019 à 18:07
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mywishlist`
--

-- --------------------------------------------------------

--
-- Structure de la table `cagnotte`
--

DROP TABLE IF EXISTS `cagnotte`;
CREATE TABLE IF NOT EXISTS `cagnotte` (
  `idCagnotte` int(11) NOT NULL AUTO_INCREMENT,
  `nomReservation` varchar(50) DEFAULT NULL,
  `prenomReservation` varchar(50) DEFAULT NULL,
  `prix` int(50) DEFAULT NULL,
  ` message` text,
  PRIMARY KEY (`idCagnotte`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cagnotte`
--

INSERT INTO `cagnotte` (`idCagnotte`, `nomReservation`, `prenomReservation`, `prix`, ` message`) VALUES
(33, 'LAUNOIS', 'Remy', 100, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `liste_id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `descr` text,
  `img` text,
  `url` text,
  `tarif` decimal(5,2) DEFAULT NULL,
  `nomReservation` text,
  `prenomReservation` text,
  `statutReservation` tinyint(1) DEFAULT NULL,
  `message` text,
  `urlImg` tinyint(1) DEFAULT NULL,
  `cagnotte` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `item`
--

INSERT INTO `item` (`id`, `liste_id`, `nom`, `descr`, `img`, `url`, `tarif`, `nomReservation`, `prenomReservation`, `statutReservation`, `message`, `urlImg`, `cagnotte`) VALUES
(1, 2, 'Champagne', 'Bouteille de champagne + flutes + jeux à gratter', 'champagne.jpg', '', '20.00', 'LAUNOIS', 'Rémy', 1, NULL, NULL, NULL),
(2, 2, 'Musique', 'Partitions de piano à 4 mains', 'musique.jpg', '', '25.00', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 2, 'Exposition', 'Visite guidée de l’exposition ‘REGARDER’ à la galerie Poirel', 'poirelregarder.jpg', '', '14.00', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 3, 'Goûter', 'Goûter au FIFNL', 'gouter.jpg', '', '20.00', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 3, 'Projection', 'Projection courts-métrages au FIFNL', 'film.jpg', '', '10.00', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 2, 'Bouquet', 'Bouquet de roses et Mots de Marion Renaud', 'rose.jpg', '', '16.00', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 2, 'Diner Stanislas', 'Diner à La Table du Bon Roi Stanislas (Apéritif /Entrée / Plat / Vin / Dessert / Café / Digestif)', 'bonroi.jpg', '', '60.00', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 3, 'Origami', 'Baguettes magiques en Origami en buvant un thé', 'origami.jpg', '', '12.00', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 3, 'Livres', 'Livre bricolage avec petits-enfants + Roman', 'bricolage.jpg', '', '24.00', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 2, 'Diner  Grand Rue ', 'Diner au Grand’Ru(e) (Apéritif / Entrée / Plat / Vin / Dessert / Café)', 'grandrue.jpg', '', '59.00', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 0, 'Visite guidée', 'Visite guidée personnalisée de Saint-Epvre jusqu’à Stanislas', 'place.jpg', '', '11.00', NULL, NULL, NULL, NULL, NULL, NULL),
(12, 2, 'Bijoux', 'Bijoux de manteau + Sous-verre pochette de disque + Lait après-soleil', 'bijoux.jpg', '', '29.00', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 0, 'Jeu contacts', 'Jeu pour échange de contacts', 'contact.png', '', '5.00', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 0, 'Concert', 'Un concert à Nancy', 'concert.jpg', '', '17.00', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 1, 'Appart Hotel', 'Appart’hôtel Coeur de Ville, en plein centre-ville', 'apparthotel.jpg', '', '56.00', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 2, 'Hôtel d\'Haussonville', 'Hôtel d\'Haussonville, au coeur de la Vieille ville à deux pas de la place Stanislas', 'hotel_haussonville_logo.jpg', '', '169.00', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 1, 'Boite de nuit', 'Discothèque, Boîte tendance avec des soirées à thème & DJ invités', 'boitedenuit.jpg', '', '32.00', NULL, NULL, NULL, NULL, NULL, NULL),
(27, 1, 'Fort Aventure', 'Découvrez Fort Aventure à Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut à l\'élastique inversé, Toboggan géant... et bien plus encore.', 'fort.jpg', '', '25.00', '', '', 1, '', NULL, NULL),
(185, 7, 'Apple XR', 'Téléphone apple', 'apple-iphone-xr.jpg', '', '700.00', 'LAUNOIS', 'Remy', 0, NULL, NULL, 1),
(186, 7, 'Doudou', 'Doudou plat', 'doudou-plat-ourson.jpg', '', '10.00', NULL, NULL, 0, NULL, NULL, NULL),
(187, 8, 'Cadeaux', 'Des cadeaux surprises', 'cadeaux.jpg', '', '10.00', NULL, NULL, 0, NULL, 0, NULL),
(188, 8, 'Item avec url image', 'Lumiere', 'https://vignette.wikia.nocookie.net/princessdisney/images/c/c7/Lumi%C3%A8re.png/revision/latest?cb=20130821114850', '', '5.00', 'LAUNOIS', 'Remy', 0, 'Super!', 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

DROP TABLE IF EXISTS `liste`;
CREATE TABLE IF NOT EXISTS `liste` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `expiration` date DEFAULT NULL,
  `tokenAdmin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nomCreateur` text COLLATE utf8_unicode_ci,
  `tokenParticipant` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `privacy` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `etat` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `liste`
--

INSERT INTO `liste` (`no`, `user_id`, `titre`, `description`, `expiration`, `tokenAdmin`, `nomCreateur`, `tokenParticipant`, `privacy`, `etat`) VALUES
(1, 1, 'Pour fêter le bac !', 'Pour un week-end à Nancy qui nous fera oublier les épreuves. ', '2020-06-27', '1234', NULL, '4321', NULL, 'F'),
(2, 2, 'Liste de mariage d\'Alice et Bob', 'Nous souhaitons passer un week-end royal à Nancy pour notre lune de miel :)', '2018-06-30', 'abcd', NULL, 'dcba', NULL, 'F'),
(3, 3, 'C\'est l\'anniversaire de Charlie', 'Pour lui préparer une fête dont il se souviendra :)', '2017-12-12', '1a', NULL, 'a1', NULL, 'F'),
(7, NULL, 'ExempleListe', 'Demo', '2025-06-08', '9a081ce04a29cb61f45a8124bb3e5f884b2767c4792a36db1be24a2b1da12142', NULL, '19bcdc60f85bef979dd9649c5ca831ba152596a32f1af4f05a4af9a4c3b74e75', NULL, 'F'),
(8, NULL, 'ListeAmoi', 'Privé', '2022-01-01', 'ad04f7e29aa01ccd03ce88711602eaab42c2c9833caab2a4b892185a2ea22f2f', NULL, '51ac16ad0527f4847bd1fcf08b5b3719968566c145a016a0186dc4149ade2f2a', 'X', 'F');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id_message` int(11) NOT NULL AUTO_INCREMENT,
  `id_liste` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `nom` text NOT NULL,
  `prenom` text,
  `message` text,
  PRIMARY KEY (`id_message`),
  KEY `id_liste` (`id_liste`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `message`
--

INSERT INTO `message` (`id_message`, `id_liste`, `id_user`, `nom`, `prenom`, `message`) VALUES
(2, 8, NULL, 'LAUNOIS', 'Remy', 'Belle liste');

-- --------------------------------------------------------

--
-- Structure de la table `articipeCagnotte`
--

DROP TABLE IF EXISTS `participeCagnotte`;
CREATE TABLE IF NOT EXISTS `participeCagnotte` (
  `id` int(11) NOT NULL,
  `idCagnotte` int(11) NOT NULL,
  PRIMARY KEY (`idCagnotte`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `participeCagnotte`
--

INSERT INTO `participeCagnotte` (`id`, `idCagnotte`) VALUES
(185, 33);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `nomUser` text NOT NULL,
  `prenomUser` text NOT NULL,
  `mailUser` varchar(200) NOT NULL,
  `mdpUser` text NOT NULL,
  `tokenUser` text,
  PRIMARY KEY (`idUser`),
  UNIQUE KEY `mailUser` (`mailUser`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`id_liste`) REFERENCES `liste` (`no`),
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`idUser`);

--
-- Contraintes pour la table `participeCagnotte`
--
ALTER TABLE `participeCagnotte`
  ADD CONSTRAINT `participeCagnotte_ibfk_1` FOREIGN KEY (`id`) REFERENCES `item` (`id`),
  ADD CONSTRAINT `participeCagnotte_ibfk_2` FOREIGN KEY (`idCagnotte`) REFERENCES `cagnotte` (`idCagnotte`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
