<?php

namespace wishlist\views;


use Slim\Slim;

/**
 * Created by PhpStorm.
 * User: quent
 * Date: 05/12/2018
 * Time: 13:48
 */
class VueIndex extends Vue
{

    const RIEN = 0;
    const INDEX = 1;
    private $header, $body, $content, $selecteur;

    public function __construct($select = 0)
    {
        $this->selecteur = $select;

    }

    /**
     * Appel des differentes methodes pour l affichage d une page HTML
     * @return string la page generee
     */
    public function render()
    {
        switch ($this->selecteur) {
            case VueIndex::RIEN :
                {
                    $this->body = $this->content;
                    break;
                }
            case VueIndex::INDEX :
                {
                    $this->body = $this->index();
                    break;
                }
        }
        $html = parent::sethtml($this->body);
        return $html;
    }

    /**
     * methode affichant la page principale du site web
     * @return string la vue
     */
    public function index(){
        $app = Slim::getInstance();
        $formatage = "
        
        <br>
        <div class='row justify-content-center'>
        <fieldset  style=\"border:solid 1px cadetblue; padding: 3%; width:40%;\"> 
   <legend class='creer'>Créer votre liste </legend> 
   <div class=\"row\">
   <a href=\"" . $app->urlFor('listcreer') . "\" class=\"btn btn-outline-info btn-lg btn-lg btn-block\" role=\"button\" aria-pressed=\"true\">Creer votre liste</a></div>
    </fieldset></div>

    <br>

<div class='row justify-content-center'>
        <fieldset  style=\"border:solid 1px seagreen; padding: 3%; width:40%;\"> 
   <legend class='listes'>Listes publiques</legend> 
   <div class=\"row\">
   <a href=\"" . $app->urlFor('listeItem') . "\" class=\"btn btn-outline-success btn-lg btn-lg btn-block\" role=\"button\" aria-pressed=\"true\">Listes publiques</a></div>
    </fieldset></div>
        <br>
        ";


        if (isset($_SESSION['auth'])) {
            $formatage.="<div class='row justify-content-center'>
        <fieldset  style=\"border:solid 1px black; padding: 3%; width:40%;\"> 
   <legend class='deco'>Deconnexion</legend> 
   <div class=\"row\">
   <a href=\"" . $app->urlFor('deconnexion') . "\" class=\"btn btn-outline-dark btn-lg btn-lg btn-block\" role=\"button\" aria-pressed=\"true\">Se deconnecter</a></div>
    </fieldset></div>";
        }else{
            $formatage.="
        
        <div class='row justify-content-center'>
        <fieldset  style=\"border:solid 1px black; padding: 3%; width:40%;\"> 
   <legend class='co'>Connexion</legend> 
   <div class=\"row\">
   <a href=\"" . $app->urlFor('connexion') . "\" class=\"btn btn-outline-dark btn-lg btn-lg btn-block\" role=\"button\" aria-pressed=\"true\">Se connecter</a></div>
    </fieldset></div>";}
        return $formatage;
    }


}



