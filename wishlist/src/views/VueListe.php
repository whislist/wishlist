<?php
/**
 * Created by PhpStorm.
 * User: palauclement
 * Date: 12/12/2018
 * Time: 19:54
 */

namespace wishlist\views;


class VueListe extends Vue
{

    const RIEN = 0;
    const CREATE_LIST = 1;
    const AJOUT_ITEM = 2;
    const MODIF_ITEM = 3;
    const MODIF_LIST = 4;
    private $header, $body, $content, $selecteur, $args;


    public function __construct($select = 0, $args = [])
    {
        $this->selecteur = $select;
        $this->args = $args;

    }

    /**
     * Appel des differentes methodes pour l affichage d une page HTML
     * @return string la page generee
     */
    public function render()
    {
        switch ($this->selecteur) {
            case VueListe::RIEN :
                {
                    $this->body = $this->content;
                    break;
                }
            case VueListe::CREATE_LIST :
                {
                    $this->body = $this->createList();
                    break;
                }

            case VueListe::AJOUT_ITEM :
                {
                    $this->body = $this->addItem();
                    break;
                }

            case VueListe::MODIF_ITEM :
                {
                    $this->body = $this->modification_item();
                    break;
                }

            case VueListe::MODIF_LIST :
                {
                    $this->body = $this->modification_liste();
                    break;
                }

        }
        $html = parent::sethtml($this->body);
        return $html;
    }

    /**
     * Methode pour le formulaire de creation d une liste
     * @return string
     */
    private function createList()
    {
        $formatage = "";
        $formatage .= " 
<h2 class=\"text-center font-weight-bold\" style='font-family: \"Comic Sans MS\"; background: #4D4D4D; font-size: 18px; font-weight: 100; padding: 20px; color: #797979;'>Créer une liste</h2>
  <form method='post'>
  <div class=\"input-group mb-2\" style='width: 90%;'>
    <div class=\"input-group-prepend ml-3\">
        <span class=\"input-group-text\" id=\"basic-addon1\">Nom de la liste <i>*</i></span>
    </div>
    <input type=\"text\" name='titre' class=\"form-control\" required>
     <div class=\"input-group-append\">
        <div class=\"btn-group-toggle\" data-toggle=\"buttons\">
           <div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\">
  <label class=\"btn btn-outline-danger active\">
    <input type=\"radio\"  id=\"customRadioInline1\" name=\"privacy\" class=\"custom-control-input\" value='public' checked> Public 
  </label>
  <label class=\"btn btn-outline-danger\">
    <input type=\"radio\" id=\"customRadioInline2\" name=\"privacy\" class=\"custom-control-input\" value='prive'> Privé
  </label>
</div>
        </div>
    </div>
  </div>
  
  
  <hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
  
  <div class=\"input-group mb-2\" style='width: 90%;'>
    <div class=\"input-group-prepend ml-3\">
        <span class=\"input-group-text\">Nom createur </span>
    </div>
    <input type=\"text\" name='#' class=\"form-control\">
  </div>
 
 <hr style='border-top: 1px solid #333; width: 89%; align-items: center; '>  
 
 
 <div class=\"input-group mb-2\" style='width: 90%;'>
    <div class=\"input-group-prepend ml-3\">
        <span class=\"input-group-text\">Date d'expiration <i>*</i></span>
    </div>
    <input type=\"date\" name='expiration' class=\"form-control\" required>
  </div>
 
 <hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
 
  <div class=\"input-group mb-2\" style='width: 90%;'>
    <div class=\"input-group-prepend ml-3\">
        <span class=\"input-group-text\">Description de la liste <i>*</i></span>
    </div>
     <input type=\"text\" name='description' class=\"form-control\" required>
  </div>
 
<hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
 
  <div class=\"row justify-content-start\">
<div class=\"col ml-5 mr-5\">
<div class='ml-3 mr-3'>
  <button type='submit' name='valider_inc' value='valid_f1' class=\"btn btn-primary btn-lg btn-block\">Valider</button></div></div></div>
</form>  ";
        return $formatage;
    }

    /**
     * Methode pour le rendu de l ajout d un item
     * @return string
     */
    private function addItem()
    {
        $formatage = "";
        if (isset($this->args['error']) && !empty($this->args['error'])) {
            $formatage .= "<div class=\"alert alert-warning\">
                             <strong>Warning!</strong> " . $this->args['error'] . "
                            </div>";

        }
        $formatage .= "<h2 class=\"text-center font-weight-bold\" style='font-family: \"Comic Sans MS\"; background: #4D4D4D; font-size: 18px; font-weight: 100; padding: 20px; color: #797979;'>Créer un item</h2>
  <form method='post' onsubmit='changeBouton()' enctype=\"multipart/form-data\">
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\" >Nom de l'item <i>*</i></span>
  </div>
  <input type=\"text\" name='nom' class=\"form-control\" placeholder='Exemple : Pikachu' required>
</div>

<hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
    
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\">Prix de l'item € <i>*</i></span>
  </div>
    <input type=\"text\" name='tarif' class=\"form-control\" placeholder='Exemple : 120' required>
    <div class=\"input-group-append\">
        <div class=\"btn-group-toggle\" data-toggle=\"buttons\">
            <label class=\"btn btn-outline-danger\">
                 <input type=\"checkbox\" name=\"cagnotte\" class=\"form-check-input\" id=\"exampleCheck1\"> Item a cagnotte
            </label>
        </div>
    </div>
  </div>
  
  <hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
  
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\"> Url de l'item (lien)</span>
  </div>
    <input type=\"text\" name='url' class=\"form-control\" placeholder='Exemple : https://www.amazon.fr/Philips-machine-cafe'>
  </div>
  
  <hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
  
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\">Description de l'item<i>*</i></span>
  </div>
    <input type=\"text\" name='descr' class=\"form-control\" placeholder='Exemple : Ce pikachu est super pour battre tous les dresseurs !!' required>
  </div>
  
  <hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 


<div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-5\">
<div class=\"custom-control custom-radio custom-control-inline ml-5 mr-5\">
<div class='ml-5'>
  <input type=\"radio\"  id=\"customRadioInline1\" name=\"choix\" class=\"custom-control-input\" value='burlImg' onclick='f()'>
  <label class=\"custom-control-label\" for=\"customRadioInline1\">Image avec url</label>
  </div>
</div>

<div class=\"custom-control custom-radio custom-control-inline ml-5 mr-5\">
  <input type=\"radio\" id=\"customRadioInline2\" name=\"choix\" class=\"custom-control-input\" value='bnomImg' onclick='f()'>
  <label class=\"custom-control-label\" for=\"customRadioInline2\">Image avec fichier existant</label>
</div>

<div class=\"custom-control custom-radio custom-control-inline ml-5\">
  <input type=\"radio\" id=\"customRadioInline3\" name=\"choix\" class=\"custom-control-input\" value='bfileToUpload' onclick='f()'>
  <label class=\"custom-control-label\" for=\"customRadioInline3\">Image avec votre fichier </label>
  </div></div></div>
  
  <hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
  
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\" > Url de l'image</span>
  </div>
     <input type=\"text\" name=\"urlImg\" class=\"form-control\" id='urlImg' disabled>
  </div>
  
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\"> Nom de l'image</span>
  </div>
      <select class=\"form-control\" id=\"exampleFormControlSelect1\" name=nomImg disabled>
            
            ";


        $tab = $this->lireNomFicher();

        foreach ($tab as $v) {

            $formatage .= "<option>$v</option>";
        }

        $formatage .= "</select>  </div>

           <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\">Importer une image</span>
  </div>
  <div class=\"custom-file\">
    <input type=\"file\" class=\"custom-file-input\" name=\"fileToUpload\"  id=\"inputGroupFile01\" disabled>
    <label class=\"custom-file-label\" for=\"inputGroupFile01\">Parcourir</label>
  </div>
  </div>
  
  <hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
  


<div class=\"row justify-content-start\">
<div class=\"col ml-5 mr-5\">
<div class='ml-3 mr-3'>
  <button type='submit' name='valider_inc' value='valid_f1' class=\"btn btn-primary btn-lg btn-block\">Valider</button></div></div></div>
  </form>
<script>
function f() {
         if(document.getElementById('customRadioInline1').checked) {
                document.getElementById('urlImg').disabled = false;
                document.getElementById('inputGroupFile01').disabled = true;
                document.getElementById('exampleFormControlSelect1').disabled = true;
        }else if(document.getElementById('customRadioInline2').checked) {
                document.getElementById('urlImg').disabled = true;
                document.getElementById('inputGroupFile01').disabled = true;
                document.getElementById('exampleFormControlSelect1').disabled = false;
        }else if (document.getElementById('customRadioInline3').checked){
                document.getElementById('urlImg').disabled = true;
                document.getElementById('inputGroupFile01').disabled = false;
                document.getElementById('exampleFormControlSelect1').disabled = true;
        } 
       }
function changeBouton() {
  var bouton = document.getElementsByName('valider_inc');
  if(!confirm('Voulez être redirigé vers votre panel d\'administration ?')){
      bouton[0].value = 'valid_f1';
  }
  else {
      bouton[0].value = 'valid_f2';
  }
  
}
</script>";

        return $formatage;
    }

    /**
     * Methode qui renvoit les noms des images du site web
     * @return array
     */
    private function lireNomFicher()
    {
        $dir = "./img/";
        $tab = [];
        if (is_dir($dir)) {
            // si il contient quelque chose
            if ($dh = opendir($dir)) {
                $i = 0;
                // boucler tant que quelque chose est trouve
                while (($file = readdir($dh)) !== false) {
                    $i++;
                    // affiche le nom et le type si ce n'est pas un element du systeme
                    //"fichier : $file : type : " . filetype($dir . $file) . "<br />\n";
                    if ($file != '.' && $file != '..') {
                        array_push($tab, "$file");
                    }

                }
                // on ferme la connection
                closedir($dh);
            }
        }
        return $tab;
    }

    /**
     * Methode pour le rendu de modification d un item
     * @return string
     */
    public function modification_item()
    {
        $nomItem = $this->args['nom'];
        $description = $this->args['descr'];
        $tarif = $this->args['tarif'];
        $url = $this->args['url'];
        $formatage = "";

        if (isset($this->args['error']) && !empty($this->args['error'])) {
            $formatage .= "<div class=\"alert alert-warning\">
                             <strong>Warning!</strong> " . $this->args['error'] . "
                            </div>";

        }
        $formatage .= "<h2 class=\"text-center font-weight-bold\" style='font-family: \"Comic Sans MS\"; background: #4D4D4D; font-size: 18px; font-weight: 100; padding: 20px; color: #797979;'>Modifier un item</h2>
  <form method='post' onsubmit='changeBouton()' enctype=\"multipart/form-data\">
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\">Nom de l'item <i>*</i></span>
  </div>
  <input type=\"text\" name='nom' class=\"form-control\" placeholder='Exemple : Pikachu' value='$nomItem' autocomplete='on' required>
</div>

<hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
    
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\">Prix de l'item € <i>*</i></span>
  </div>
    <input type=\"text\" name='tarif' class=\"form-control\" placeholder='Exemple : 120' value='$tarif' autocomplete='on' required>

  </div>
  
<hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
  
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\"> Url de l'item (lien)</span>
  </div>
    <input type=\"text\" name='url' class=\"form-control\" placeholder='Exemple : https://www.amazon.fr/Philips-machine-cafe' value='$url' autocomplete='on'>
  </div>
  
  <hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
  
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\" id=\"basic-addon1\">Description de l'item<i>*</i></span>
  </div>
    <input type=\"text\" name='descr' class=\"form-control\" placeholder='Exemple : Ce pikachu est super pour battre tous les dresseurs !!' value='$description' autocomplete='on' required>
  </div>
  
  <hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 


<div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-5\">
<div class=\"custom-control custom-radio custom-control-inline ml-5 mr-5\">
<div class='ml-5'>
  <input type=\"radio\"  id=\"customRadioInline1\" name=\"choix\" class=\"custom-control-input\" value='burlImg' onclick='f()'>
  <label class=\"custom-control-label\" for=\"customRadioInline1\">Image avec url</label>
  </div>
</div>

<div class=\"custom-control custom-radio custom-control-inline ml-5 mr-5\">
  <input type=\"radio\" id=\"customRadioInline2\" name=\"choix\" class=\"custom-control-input\" value='bnomImg' onclick='f()'>
  <label class=\"custom-control-label\" for=\"customRadioInline2\">Image avec fichier existant</label>
</div>

<div class=\"custom-control custom-radio custom-control-inline ml-5\">
  <input type=\"radio\" id=\"customRadioInline3\" name=\"choix\" class=\"custom-control-input\" value='bfileToUpload' onclick='f()'>
  <label class=\"custom-control-label\" for=\"customRadioInline3\">Image avec votre fichier </label>
  </div></div></div>
  
  <hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 
  
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\"> Url de l'image</span>
  </div>
     <input type=\"text\" name=\"urlImg\" class=\"form-control\" id='urlImg' disabled>
  </div>
  
  <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\"> Nom de l'image</span>
  </div>
      <select class=\"form-control\" id=\"exampleFormControlSelect1\" name=nomImg disabled>
            
            ";


        $tab = $this->lireNomFicher();

        foreach ($tab as $v) {

            $formatage .= "<option>$v</option>";
        }

        $formatage .= "</select>  </div>

           <div class=\"input-group mb-2 ml-5\" style='width: 90%;'>
  <div class=\"input-group-prepend ml-3\">
    <span class=\"input-group-text\">Importer une image</span>
  </div>
  <div class=\"custom-file\">
    <input type=\"file\" class=\"custom-file-input\" name=\"fileToUpload\"  id=\"inputGroupFile01\" disabled>
    <label class=\"custom-file-label\" for=\"inputGroupFile01\">Parcourir</label>
  </div>
  </div>
  
<hr style='border-top: 1px solid #333; width: 89%; align-items: center; '> 

<div class=\"row justify-content-start\">
<div class=\"col ml-5 mr-5\">
<div class='ml-3 mr-3'>
  <button type='submit' name='valider_inc' value='valid_f1'  class=\"btn btn-primary btn-lg btn-block\">Valider</button></div></div></div>
  </form>
<script>
function f() {
         if(document.getElementById('customRadioInline1').checked) {
                document.getElementById('urlImg').disabled = false;
                document.getElementById('inputGroupFile01').disabled = true;
                document.getElementById('exampleFormControlSelect1').disabled = true;
        }else if(document.getElementById('customRadioInline2').checked) {
                document.getElementById('urlImg').disabled = true;
                document.getElementById('inputGroupFile01').disabled = true;
                document.getElementById('exampleFormControlSelect1').disabled = false;
        }else if (document.getElementById('customRadioInline3').checked){
                document.getElementById('urlImg').disabled = true;
                document.getElementById('inputGroupFile01').disabled = false;
                document.getElementById('exampleFormControlSelect1').disabled = true;
        } 
       }
function changeBouton() {
  var bouton = document.getElementsByName('valider_inc');
  if(!confirm('Voulez être redirigé vers votre panel d\'administration ?')){
      bouton[0].value = 'valid_f1';
  }
  else {
      bouton[0].value = 'valid_f2';
  }
  
}
</script>";

        return $formatage;
    }

    /**
     * Methode pour le rendu de modification d une liste
     * @return string
     */
    public function modification_liste()
    {
        $titre = $this->args['titre'];
        $nomCreateur = $this->args['nomCreateur'];
        $expiration = $this->args['expiration'];
        $description = $this->args['descr'];
        $formatage = " <form method='post'>
  <div class=\"form-group\">
    <label>Nom de la liste :</label>
    <input type=\"text\" name='titre' class=\"form-control\" value='$titre' autocomplete='on'>
  </div>
  <div class=\"form-group\">
    <label>Nom createur :</label>
    <input type=\"text\" name='#' class=\"form-control\" value='$nomCreateur' autocomplete='on'>
  </div>
  <div class=\"form-group\">
    <label>Date d'expiration :</label>
    <input type=\"date\" name='expiration' class=\"form-control\" value='$expiration' autocomplete='on'>
  </div>
  <div class=\"form-group\">
    <label>Description de la liste :</label>
    <input type=\"text\" name='description' class=\"form-control\" value='$description' autocomplete='on'>
  </div>
  <button type=\"submit\" name='valider_inc' value='valid_f1' class=\"btn btn-default\">Valider</button>
</form>  ";

        return $formatage;
    }
}