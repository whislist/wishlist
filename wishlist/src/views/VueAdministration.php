<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 23/12/2018
 * Time: 22:44
 */

namespace wishlist\views;


use Slim\Slim;

class VueAdministration extends Vue
{
    const RIEN = 0;
    const LIST_ITEM = 1;
    const ITEM_VIEW = 2;

    public $body, $content, $selecteur, $args;

    public function __construct($item, $select = 0, $args = [])
    {
        $this->content = $item;
        $this->args = $args;
        $this->selecteur = $select;

    }

    /**
     * Appel des differentes méthodes pour l affichage d une page HTML
     * @return string page generee
     */
    public function render()
    {
        switch ($this->selecteur) {
            case VueAdministration::RIEN :
                {
                    $this->body = $this->content;
                    break;
                }
            case VueAdministration::LIST_ITEM :
                {
                    $this->body = $this->item_gestion() . $this->buttonAddItem();
                    break;
                }
            case VueAdministration::ITEM_VIEW :
                {
                    $this->body = $this->htmlItem();
                    break;
                }

        }
        $html = parent::sethtml($this->body);
        return $html;

    }

    /**
     * Affichage des items d une liste pour un createur
     * @return string
     */
    public function item_gestion()
    {
        $app = Slim::getInstance();

        $formatage = "
    <div class=\"table-responsive\">
        <table class=\"table table-bordered\"><thead>
    <tr>
      <th scope=\"col\">Nom item</th>
      <th scope=\"col\">Modifier item</th>
      <th scope=\"col\">Supprimer item</th>
      <th scope=\"col\">Suprimer image</th>
      <th scope=\"col\">Message</th>
    </tr>
  </thead>";


        foreach ($this->content as $key => $value) {

            $formatage .= "<tr>
                        <th> $value[nom] </th>";
            if ($value['statutReservation'] == TRUE || $value['cagnotte'] == TRUE) {
                $formatage .= " <th><button type=\"button\" class=\"btn btn-outline-secondary\" disabled>Reserve</button></th><th></th><th></th><th></th>";
            } else {
                $formatage .= "<th><a class=\"btn btn-outline-success\" href=\"" . $app->urlFor('modifItem', array('token' => $this->args['token'], 'id' => $value['id'])) . "\" role=\"button\">Modifier</a></th>";

                $link = $app->urlFor('listeAdm', array('token' => $this->args['token']));
                $id = $value['id'];

                $formatage .= "<th> <form method=\"post\" > <button type=\"submit\" class=\"btn btn-outline-danger\" name='supprimer' value=\"supprimer_$id\">Supprimer</button> </form></th>";
                $formatage .= "<th> <form method=\"post\" > <button type=\"submit\" class=\"btn btn-outline-danger\" name='supprimerImg' value=\"supprimerImg_$id\">Supprimer Image</button> </form></th>";


                if ("$value[message]" != null) {
                    if ($this->args['expiration'] < date('Y-m-d')) {
                        $formatage .= "<th><button type=\"button\" class=\"btn btn-outline-primary\" data-toggle=\"modal\" data-target=\"#$value[message]\">Message</button></th>";
                    }
                } else {
                    $formatage .= "<th></th>";
                }
                //<button type="submit" name='valider_inc' value='valid_f1' >Valider</button>
                $formatage .= $this->see_message("$value[nomReservation]", "$value[prenomReservation]", "$value[message]");
            }

            $formatage .= "</tr>";
        }

        $formatage .= "</TABLE></div>";
        return $formatage;
    }

    /**
     * Methode pour afficher le message d un participant a un createur
     * @param $nom string nom du participant
     * @param $prenom string prenom du participant
     * @param $message string message du participant
     * @return string
     */
    public function see_message($nom, $prenom, $message)
    {

        $formatage = "";
        $expiration = $this->args['expiration'];
        $today = date('Y-m-d');
        $user_id = $this->args['user_id'];
        $iduser = $this->args['auth'];
        if ($expiration < $today) {

            if ($user_id == $iduser) {
                $formatage = "
<div class=\"modal fade\" id=\"$message\" tabindex=\"0\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Message de $prenom $nom</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">" .
                    "$message" . "
      </div>
    </div>
  </div>
</div>";
            }

        }

        return $formatage;
    }

    /**
     * Methode pour le bouton ajouter un item
     * @return string
     */
    private function buttonAddItem()
    {

        $expiration = $this->args['expiration'];
        $tokenParticipant = $this->args['tokenPart'];
        $today = date('Y-m-d');


        $app = Slim::getInstance();
        $link = $app->urlFor('liste/ajout', array('token' => $this->args['token']));
        $formatage = "";
        if ($this->args['etat'] == 'C') {
            $formatage = " 
           <form method=\"post\">
           <label for=\"titre\">Valider votre liste pour que tout le monde la voit :</label>
            <button type=\"submit\" name='valider'>Valider</button>
          </form>";
        } else {
            $formatage = "
            <!-- Button trigger modal -->
            <div class='row justify-content-md-center'>
            <div class=\"col col-lg-2\">
<button type=\"button\" class=\"btn btn-outline-dark\" data-toggle=\"modal\" data-target=\"#exampleModal\"  >Partager sa liste</button></div>

<!-- Modal -->
<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-lg\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">L'url de participation est</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
        <input class='form-control' id=\"post-shortlink\" value=\"" . $app->urlFor('liste', array('token' => $tokenParticipant)) . "\">
        <button type=\"button\" class='btn btn-primary' id=\"copy-button\" data-clipboard-target=\"#post-shortlink\" data-dismiss=\"modal\" style='float: right'>Copier</button>
      </div>
    </div>
  </div>
</div>";
        }
        if ($expiration > $today) {
            $formatage .= "<form method=\"get\" action=\"" . $link . "\"><div class=\"col col-lg-2\">
            <button type=\"submit\" class=\"btn btn-outline-info\" >Ajouter un item</button></div>
          </form>";
        }
        $formatage .= "<form method=\"get\" action=\"" . $app->urlFor('modifListe', array('token' => $this->args['token'])) . "\">
                        <div class=\"col col-lg-2\"><button type=\"submit\" class=\"btn btn-outline-info\" >Modifier la liste</button></div>
                        </form>
                        </div>
    <!-- 2. Include library -->
    <script src=\"/dist/clipboard.min.js\"></script>

    <!-- 3. Instantiate clipboard by passing a string selector -->
    <script>
    var clipboard = new ClipboardJS('.btn');
    clipboard.on('success', function(e) {
        alert('Lien copié');
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script>";
        /*
        if($this->args['etat'] === 'C'){
          $liste =  Liste::where('tokenAdmin','=',$token)->first();
          $liste->etat = 'F';
          $liste->save();
        }
        */
        return $formatage;

    }

    /**
     * Methode affichant un item
     * @return string
     */
    private function htmlItem()
    {
        $app = Slim::getInstance();
        if ($this->content['urlImg'] == false) {
            $src = $app->request->getRootUri() . "/img/" . $this->content['img'];
        } else {
            $src = $this->content['img'];
        }
        $formatage = "<div class=\"container\">
      <h2>" . $this->content['nom'] . "</h2>
      <p>" . $this->content['descr'] . " Tarif : " . $this->content['tarif'] . "</p>            
      <img src=$src class=\"rounded img-fluid mx-auto d-block\"  alt=\"" . $this->content['nom'] . "\" style='width: 50%;' >
    </div>";
        return $formatage;
    }


}