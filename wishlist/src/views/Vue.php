<?php

namespace wishlist\views;

use Slim\Slim;

/**
 * Created by PhpStorm.
 * User: quent
 * Date: 05/12/2018
 * Time: 13:48
 * Classe qui centralise le code html
 */
abstract class Vue
{
    /**
     * Header des pages HTML
     * @var string
     */
    private static $head = " <!DOCTYPE html>
                    <html>
                    <head>
                        <title>Whislist</title>
                        <meta charset=\"utf-8\" />
                        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
                        <link rel=\"stylesheet\" href=\"../../allcss.css\" />
                        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\"/> 
                    </head>
                    <body>";
    /**
     * Footer des pages HTML
     * @var string
     */
    private static $fin = "</div> 
                </div>
               </div>
               <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
               <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>
               <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>
            </body></html>";
    private $html;

    /**
     * Assemblage des différentes parties de la page HTML
     * @param $body
     * @return string
     */
    public function sethtml($body)
    {
        $this->html = self::$head . $this->navbar() . $this->css() . $body . self::$fin;
        return $this->html;
    }

    /**
     * Barre de navigation
     * @return string
     */
    private function navbar()
    {
        $app = Slim::getInstance();
        $navbar = " <nav class=\"navbar navbar-expand-lg fixed-top navbar-dark bg-dark position-sticky\">
       <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
       <span class=\"navbar-toggler-icon\"></span>
       </button>
       <div class=\"collapse navbar-collapse mx-auto\" id=\"navbarSupportedContent\">
       
        <div class=\"mx-auto\">
           <ul class=\"navbar-nav ml-4 mt-auto\">
           <li>
            <a class=\"navbar-brand mr-4\" href=\"" . $app->request->getRootUri() . "/\">Wishlist</a>
            </li>
               <li class=\"nav-item active mr-2\">
                   <a class=\"nav-link\" href=\"" . $app->request->getRootUri() . "/\">Home <span class=\"sr-only\">(current)</span></a>
               </li>";
        if (isset($_SESSION['auth'])) {
            $navbar .= "<li class=\"nav-item mr-2\">
                   <a class=\"nav-link\" href=\"" . $app->urlFor('mesListes') . "\">Mes Listes</a>
               </li>
               <li class=\"nav-item mr-2\">
                   <a class=\"nav-link\" href=\"" . $app->urlFor('modification') . "\">Modifier son compte</a>
                  </li>
                 <li class=\"nav-item mr-2\">
                   <a class=\"nav-link\" href=\"" . $app->urlFor('deconnexion') . "\">Se déconnecter</a>
                  </li>
                  ";
        } else {
            $navbar .= "<li class=\"nav-item mr-2\">
                   <a class=\"nav-link\" href=\"" . $app->urlFor('connexion') . "\">Connexion</a>
                    </li>";
        }

        $navbar .= "<li class=\"nav-item mr-2\">
                   <a class=\"nav-link\" href=\"" . $app->urlFor('listcreer') . "\">Créer une liste</a>
               </li>
               <li class=\"nav-item mr-2\">
                   <a class=\"nav-link\" href=\"" . $app->urlFor('listeItem') . "\">Voir les listes publiques</a>
               </li>
               <li>
               <form method='get' action=\"" . $app->urlFor('listeItem') . "\" class=\"form-inline my-2 my-lg-0 ml-1\">
                <input name='recherche' class=\"form-control mr-sm-2\" type=\"search\" placeholder=\"Nom de la liste\" aria-label=\"Search\" style=\"font-style:italic\">
                <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"submit\">Chercher une liste</button> 
                </form></li>
           </ul>
           </div>
       </div>
   </nav>";
        return $navbar;
    }

    /**
     * Vue contenant l image de fond
     * @return string
     */
    private function css()
    {
        $app = Slim::getInstance();
        return "<div style='height: 100%; position: relative'>
                <div class=\"header-filter\" style='height: 300px; display: block; background:  url(\"" . $app->request->getRootUri() . "/img/1.jpg" . "\") fixed no-repeat; background-size: cover'> </div>
                <div class='container' style='margin-top: -5%'>
                 <div id='containerPrinc' class='shadow-lg bg-white rounded position-static' style='height: 100%'>";
    }

}