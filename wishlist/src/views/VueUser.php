<?php
/**
 * Created by PhpStorm.
 * User: wolfie
 * Date: 08/01/19
 * Time: 11:16
 */

namespace wishlist\views;

use Slim\Slim;


class VueUser extends Vue
{
    const RIEN = 0;
    const CREATE_USER = 1;
    const AUTH_USER = 2;
    const MODIFIER_USER = 3;

    private $header, $body, $content, $selecteur, $args;


    public function __construct($select = 0, $args = [])
    {
        $this->selecteur = $select;
        $this->args = $args;
    }

    /**
     * Appel des differentes methodes pour l affichage d une page HTML
     * @return string la page generee
     */
    public function render()
    {
        switch ($this->selecteur) {
            case VueUser::RIEN :
                {
                    $this->body = $this->content;
                    break;
                }
            case VueUser::CREATE_USER :
                {
                    $this->body = $this->createUser();
                    break;
                }
            case VueUser::MODIFIER_USER :
                {
                    $this->body = $this->modifyUser();
                    break;
                }
            case VueUser::AUTH_USER :
                {
                    $this->body = $this->authentification();
                    break;
                }

        }
        $html = parent::sethtml($this->body);
        return $html;
    }

    /**
     * Methode d affichage du formulaire de creation d un utilisateur
     * @return string
     */
    private function createUser()
    {
        $app = Slim::getInstance();
        $formatage = "";
        if (isset($this->args['error']) && !empty($this->args['error'])) {
            $formatage .= "<div class=\"alert alert-warning\">
                             <strong>Warning!</strong> " . $this->args['error'] . "
                            </div>";

        }
        $formatage .= "   

  <form method='post' action=\"" . $app->urlFor('inscription') . "\">
  <div class=\"form-group\">
    <label for=\"nom\">Nom :</label>
    <input type=\"text\" name='nom' class=\"form-control\" required>
  </div>
  <div class=\"form-group\">
    <label for=\"prenom\">Prénom :</label>
    <input type=\"text\" name='prenom' class=\"form-control\"  required>
  </div>
  <div class=\"form-group\">
    <label for=\"mail\">Mail :</label>
    <input type=\"email\" name='mail' class=\"form-control\" required>
  </div>
  <div class=\"form-group\">
    <label for=\"mail_confirm\">Confirmez votre Mail :</label>
    <input type=\"email\" name='mail_confirm' class=\"form-control\" required>
  </div>
   <div class=\"form-group\">
    <label for=\"mdp\">Mot de passe :</label>
    <input type=\"password\" name='mdp' class=\"form-control\" required>
  </div>
  <div class=\"form-group\">
    <label for=\"mdp_confirm\">Confirmez votre mot de passe :</label>
    <input type=\"password\" name='mdp_confirm'  class=\"form-control\" required>
  </div>

  <button type=\"submit\" name='inscription_inc' value='inscription_f1' class=\"btn btn-default\">Valider</button>
</form>  ";
        return $formatage;
    }

    /**
     * Methode affichant le formulaire de modification d un utilisateur
     * @return string la vue
     */
    public function modifyUser()
    {
        $app = Slim::getInstance();
        $formatage = "";
        if (isset($this->args['error']) && !empty($this->args['error'])) {
            $formatage .= "<div class=\"alert alert-warning\">
                             <strong>Warning!</strong> " . $this->args['error'] . "
                            </div>";

        }
        $formatage .= "  

  <form method='post' action=\"" . $app->urlFor('modification') . "\">
  <div class=\"form-group\">
    <label for=\"nom\">Nom :</label>
    <input type=\"text\" name='nom' id='nom' class=\"form-control\" value=" . $this->args[0] . " required>
  </div>
  <div class=\"form-group\">
    <label for=\"prenom\">Prénom :</label>
    <input type=\"text\" name='prenom' id='prenom' class=\"form-control\" value=" . $this->args[1] . " required>
  </div>
  <div class=\"form-group\">
    <label for=\"mdp\">Ancien mot de passe :</label>
    <input type=\"password\" name='oldmdp' id='mdp' class=\"form-control\" >
  </div>
   <div class=\"form-group\">
    <label for=\"newmdp\">Nouveau mot de passe :</label>
    <input type=\"password\" name='mdp' id='newmdp' class=\"form-control\" >
  </div>
  <div class=\"form-group\">
    <label for=\"mdp_confirm\">Confirmez votre nouveau mot de passe :</label>
    <input type=\"password\" name='mdp_confirm' id='mdp_confirm' class=\"form-control\" >
  </div>

  <button type=\"submit\" name='modification_inc' value='modification_f1' class=\"btn btn-default\">Valider</button>
</form>  ";
        return $formatage;
    }

    /**
     * Methode d affichage de la page de connexion d un utilisateur
     * @return string
     */
    private function authentification()
    {
        $app = Slim::getInstance();
        $formatage = "";
        if (isset($this->args['error']) && !empty($this->args['error'])) {
            $formatage .= "<div class=\"alert alert-warning\">
                             <strong>Warning!</strong> " . $this->args['error'] . "
                            </div>";

        }
        $formatage .= " 
    
  <form method='post' action=\"" . $app->urlFor('connexion') . "\">
  <div class=\"form-group\">
    <label for=\"mail\">Mail :</label>
    <input type=\"email\" name='mail' id='mail' class=\"form-control\" required>
  </div>
   <div class=\"form-group\">
    <label for=\"mdp\">Mot de passe :</label>
    <input type=\"password\" name='mdp' id='mdp' class=\"form-control\" required>
  </div>

  <button type=\"submit\" name='connexion_inc' value='connexion_f1' class=\"btn btn-default\">Se connecter</button>
</form>  
<form method='get' action=\"" . $app->urlFor('inscription') . "\">
<button type=\"submit\" class=\"btn btn-default\">S'inscrire</button>
</form>
";
        return $formatage;
    }
}