<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 11/12/2018
 * Time: 12:31
 */

namespace wishlist\views;


use Slim\Slim;
use wishlist\controleurs\ControleurParticipation;

class VueParticipant extends Vue
{

    const RIEN = 0;
    const LIST_ITEM = 1;
    const ITEM_VIEW = 2;
    const LIST_LIST = 3;
    private $header, $body, $content, $selecteur, $args;


    public function __construct($item, $select = 0, $args = [])
    {
        $this->content = $item;
        $this->args = $args;
        $this->selecteur = $select;

    }

    /**
     * Appel des differentes methodes pour l affichage d une page HTML
     * @return string la page generee
     */
    public function render()
    {
        switch ($this->selecteur) {
            case VueParticipant::RIEN :
                {
                    $this->body = $this->content;
                    break;
                }
            case VueParticipant::LIST_ITEM :
                {
                    $this->body = $this->listeItem();
                    break;
                }
            case VueParticipant::ITEM_VIEW :
                {
                    $this->body = $this->htmlItem();
                    break;
                }
            case VueParticipant::LIST_LIST :
                {
                    $this->body = $this->htmlAllList();
                    break;
                }
        }
        $html = parent::sethtml($this->body);
        return $html;
    }

    /**
     * Methode d affichage des items d une liste en tant que participant
     * @return string la vue
     */
    private function listeItem()
    {
        $formatage = "";
        $app = Slim::getInstance();
        foreach ($this->content as $key => $value) {
            if ($value['urlImg'] == false && $value['img'] != null) {
                $src = $app->request->getRootUri() . "/img/" . $value['img'];
            } else {
                $src = $value['img'];
            }
            $link = $app->urlFor('item', array('token' => $this->args['token'], 'id' => $value['id']));
            $formatage .= "<div class='container'>
      <h2><a href=\"" . $link . "\">" . $value['nom'] . "</a></h2>
      <p>" . $value['descr'] . " Tarif : " . $value['tarif'] . "</p>";
            if ($src != null) {
                $formatage .= "<img src=$src class=\"rounded img-fluid mx-auto d-block \"  alt=\"" . $value['nom'] . "\" style='width: 300px; height : 300px;'>";
            }
            if ($value['statutReservation']) {
                $formatage .= '<button type="button" disabled>Reservé</button>';
            } else {
                $formatage .= '<button type="button" disabled>Non Reservé</button>';
            }
            $formatage.= "</div>";

        }
        $nomRes = "";
        $prenomRes = "";
        if (isset($_COOKIE['nomReservation']) && isset($_COOKIE['nomReservation'])) {
            $nomRes = $_COOKIE['nomReservation'];
            $prenomRes = $_COOKIE['nomReservation'];
        }

        $formatage .= "
                       <form method=\"post\">
                        <div class=\"form-group\">
                            <label>Nom :</label>
                            <input type=\"text\" name=\"nom\" autocomplete=\"on\" class=\"form-control\"  value=\"$nomRes\">
                        </div>
                        <div class=\"form-group\">
                            <label>Prenom :</label>
                            <input type=\"text\" name=\"prenom\" autocomplete=\"on\" class=\"form-control\" value=\"$prenomRes\">
                        </div>
                        <div class=\"form-group\">
                            <label>Message destine au createur :</label>
                            <input type=\"text\" name=\"message\" class=\"form-control\">
                        </div>
                        <button type=\"submit\" name=\"message_send\" class=\"btn btn-default\" value=\"message_f1\">Valider</button>
                        </form>";
        if (isset($this->args['message'])) {
            $formatage .= "<ul class=\"list-group\">";
            foreach ($this->args['message'] as $message) {
                $formatage .= "<li class=\"list-group-item\">" . $message['message'] . "</li>";
            }
            $formatage .= "</ul>";
        }

        return $formatage;
    }

    /**
     * Methode d affichage d un item comme participant
     * @return string la vue
     */
    private function htmlItem()
    {
        $app = Slim::getInstance();
        $formatage = "";
        $bool = $this->content['cagnotte'];
        if ($this->content['urlImg'] == false && $this->content['img'] != null) {
            $src = $app->request->getRootUri() . "/img/" . $this->content['img'];
        } else {
            $src = $this->content['img'];
        }
        if (isset($this->args['error']) && !empty($this->args['error'])) {
            $formatage .= "<div class=\"alert alert-warning\">
                             <strong>Warning!</strong> " . $this->args['error'] . "
                            </div>";

        }
        $tarif = $this->content['tarif'];
        if ($this->content['cagnotte'] == true) {
            $tarif = $tarif - $this->args['somme'];
        }
        $formatage .= "<div class=\"container\" id ='containerItem'>
      <h2>" . $this->content['nom'] . "</h2>
      <p>" . $this->content['descr'] . " Prix restant : " . $tarif . "</p>";
        if ($src != null) {
            $formatage .= "<img src=$src class=\"rounded img-fluid mx-auto d-block\"  alt=\"" . $this->content['nom'] . "\" style='width: 50%;' >";
        }

        if ($this->content['statutReservation'] || $tarif == 0) {
            $formatage .= '<button type="button" disabled>Reservé</button>';
        } else {
            $formatage .= '<button type="button" disabled>Non Reservé</button>';
            $formatage .= "</div>
                
     <button onClick=\"reserverItemForm($bool)\" class=\"btn btn -default\" id='res' >Reserver</button>
    <script>
    function getCookie(cname) {
  var name = cname + \"=\";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return \"\";
    }
    function reserverItemForm(booleen){ 
        var bouton = document.getElementById('res');
        if (!document.getElementById('ajout_res')){
        
        var prev = bouton.previousElementSibling;
        var nouvBouton = bouton.cloneNode(true);
        bouton.parentNode.removeChild(bouton);
        var newForm = document.createElement('form');
        newForm.method = 'post';
        newForm.id = 'ajout_res';
        var div = document.createElement('div');
        div.className = 'form-group';
        newForm.appendChild(div);
        var label = document.createElement('label');
        label.htmlFor  = 'nom';
        label.appendChild(document.createTextNode('Nom :'));
        div.appendChild(label);
        var input = document.createElement('input');
        input.type = 'text';
        input.name = 'nom';
        input.value = getCookie('nomReservation');
        input.autocomplete = 'on';
        input.className = 'form-control';
        input.required = true;
        div.appendChild(input);
        var div2 = document.createElement('div');
        div2.className = 'form-group';
        newForm.appendChild(div2);
        var label2 = document.createElement('label');
        label2.htmlFor  = 'prenom';
        label2.appendChild(document.createTextNode('Prenom :'));
        div2.appendChild(label2);
        var input2 = document.createElement('input');
        input2.type = 'text';
        input2.name = 'prenom';
        input2.value = getCookie('prenomReservation');
        input2.autocomplete = 'on';
        input2.className = 'form-control';
        input2.required = true;
        div2.appendChild(input2);
        if(!booleen){
        var div3 = document.createElement('div');
        div3.className = 'form-group';
        newForm.appendChild(div3);
        var label3 = document.createElement('label');
        label3.htmlFor  = 'message';
        label3.appendChild(document.createTextNode('Message destine au createur :'));
        div3.appendChild(label3);
        var input3 = document.createElement('input');
        input3.type = 'text';
        input3.name = 'message';
        input3.className = 'form-control';
        div3.appendChild(input3);
        }
        else{
        var div4 = document.createElement('div');
        div4.className = 'form-group';
        newForm.appendChild(div4);
        var label4 = document.createElement('label');
        label4.htmlFor  = 'prix';
        label4.appendChild(document.createTextNode('Prix :'));
        div4.appendChild(label4);
        var input4 = document.createElement('input');
        input4.type = 'text';
        input4.name = 'prix_participe';
        input4.className = 'form-control';
        div4.appendChild(input4);
        }
        
        var button = document.createElement('button');
        button.type = 'submit';
        button.name = 'reserv_inc';
        button.className = 'btn btn-default';
        button.value = 'reserv_f1';
        button.textContent = 'Valider';
        newForm.appendChild(button);
        /*newForm.appendChild(nouvBouton);
        nouvBouton.textContent = 'masquer';*/
        prev.appendChild(newForm);
        }
    }
       </script>";
        }
        return $formatage;
    }

    /**
     * Methode d affichage de toutes les listes publiques
     * @return string la vue
     */
    private function htmlAllList()
    {
        $formatage = "";
        $app = Slim::getInstance();
        $count = $this->args['count'];
        foreach ($this->content as $key => $value) {
            $link = $app->urlFor('liste', array('token' => $value['tokenParticipant']));
            $formatage .= "<div class=\"container\"> <h2><a href=\"" . $link . "\">" . $value['titre'] . "</a></h2></div>";
        }
        $page = 1;
        if (isset($_GET['page']) and intval($_GET['page'])) {
            $page = intval($_GET['page']);
        }
        if (isset($_GET['recherche'])) {
            $recherche = '&recherche=' . $_GET['recherche'];
        } else {
            $recherche = "";
        }

        if ($page - 2 < 1) {
            $pageDebut = 1;

        } else {
            $pageDebut = $page - 2;

        }
        if ($page >= 2) {
            $formatage .= "<nav aria-label=\"...\">
                      <ul class=\"pagination justify-content-center\">
                        <li class=\"page-item\">
                          <a class=\"page-link\" href=\"?page=" . strval($page - 1) . $recherche . "\">Previous</a>
                        </li>";
        } else {
            $formatage .= "<nav aria-label=\"...\">
                              <ul class=\"pagination justify-content-center\">
                                <li class=\"page-item disabled\">
                                  <a class=\"page-link\" href=\"#\" tabindex=\"-1\">Previous</a>
                                  </li>";
        }
        if ($pageDebut + 5 > ceil($count / ControleurParticipation::nbParPage)) {
            $pageFin = ceil($count / ControleurParticipation::nbParPage);
        } else {
            $pageFin = $pageDebut + 5;
        }
        for ($i = $pageDebut; $i <= $pageFin; $i++) {
            if ($i === $page) {
                $formatage .= "<li class=\"page-item active\">
                          <a class=\"page-link\" href=\"?page=" . $i . $recherche . "\">" . $i . "<span class=\"sr-only\">(current)</span></a>
                                </li>";

            } else {
                $formatage .= "<li class=\"page-item\">
                                  <a class=\"page-link\" href=\"?page=" . $i . $recherche . "\">" . $i . "</a>
                               </li>";
            }

        }

        if ($page == ceil($count / ControleurParticipation::nbParPage)) {
            $formatage .= "<li class=\"page-item disabled\">
                                  <a class=\"page-link\" href=\"#\" tabindex=\"-1\">Next</a>
                                  </li>";
        } else {
            $formatage .= "<li class=\"page-item\">
                          <a class=\"page-link\" href=\"?page=" . strval($page + 1) . $recherche . "\">Next</a>
                        </li>";
        }
        $formatage .= "
                      </ul>
                    </nav>";
        return $formatage;
    }


}