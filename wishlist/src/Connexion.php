<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 19/11/2018
 * Time: 15:47
 */

namespace wishlist;

use Illuminate\Database\Capsule\Manager as DB;


class Connexion
{
    public static $tabInit, $db;

    public static function setConfig($file)
    {
        static::$tabInit = parse_ini_file($file);
    }

    /**
     * methode utilisee par Eloquent pour la connexion a la base de donnees
     */
    public static function makeConnection()
    {
        $db = new DB();
        $db->addConnection([
            'driver' => static::$tabInit['driver'],
            'host' => static::$tabInit['host'],
            'database' => static::$tabInit['database'],
            'username' => static::$tabInit['user'],
            'password' => static::$tabInit['password'],
            'unix_socket' => static::$tabInit['unix_socket'],
            'charset' => 'UTF8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => ''
        ]);
        $db->setAsGlobal();
        $db->bootEloquent();


    }
}

