<?php
/**
 * Created by PhpStorm.
 * User: wolfie
 * Date: 18/12/18
 * Time: 11:33
 */

namespace wishlist\modele;


class User extends \Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = 'user';
    protected $primaryKey = 'idUser';

    public function user()
    {
        return $this->hasMany('wishlist\modele\Liste', 'user_id');
    }
}