<?php
/**
 * Created by PhpStorm.
 * User: wolfie
 * Date: 18/12/18
 * Time: 11:33
 */

namespace wishlist\modele;


class participeCagnotte extends \Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = 'participeCagnotte';
    protected $primaryKey = 'idCagnotte';

    public function participeCagnotte()
    {
        return $this->belongsTo('wishlist\modele\Cagnotte', 'idCagnotte');
    }

}