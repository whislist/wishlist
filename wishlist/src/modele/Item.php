<?php
/**
 * Created by PhpStorm.
 * User: Aroltir
 * Date: 19/11/2018
 * Time: 15:49
 */

namespace wishlist\modele;


class Item extends \Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = 'item';
    protected $primaryKey = 'id';

    /**
     * Permet de reconnaitre la liste de l'item
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function liste()
    {
        return $this->belongsTo('wishlist\modele\Liste', 'liste_id');
    }


}