<?php
/**
 * Created by PhpStorm.
 * User: Aroltir
 * Date: 19/11/2018
 * Time: 15:43
 */

namespace wishlist\modele;


class Liste extends \Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = 'liste';
    protected $primaryKey = 'no';

    /**
     * Permet de savoir les items d'une liste
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('wishlist\modele\Item', 'liste_id');
    }

    public function messages()
    {
        return $this->hasMany('wishlist\modele\Message', 'id_liste');
    }
}