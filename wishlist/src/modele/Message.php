<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 14/01/2019
 * Time: 22:11
 */

namespace wishlist\modele;


class Message extends \Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = 'message';
    protected $primaryKey = 'id_message';
}