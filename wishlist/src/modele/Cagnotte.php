<?php
/**
 * Created by PhpStorm.
 * User: wolfie
 * Date: 18/12/18
 * Time: 11:33
 */

namespace wishlist\modele;


class Cagnotte extends \Illuminate\Database\Eloquent\Model
{

    public $timestamps = false;
    protected $table = 'cagnotte';
    protected $primaryKey = 'idCagnotte';


    /*public function cagnotte(){
        return $this->belongsTo('wishlist\modele\Item','id');
    }*/

}