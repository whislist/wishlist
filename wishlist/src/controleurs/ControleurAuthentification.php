<?php
/**
 * Created by PhpStorm.
 * User: wolfie
 * Date: 08/01/19
 * Time: 11:07
 */

namespace wishlist\controleurs;

use Slim\Slim;
use wishlist\modele\User;
use wishlist\views\VueUser;

class ControleurAuthentification
{

    /**
     * methode affichant la page de connexion
     *
     * @param string $erreur erreur a afficher
     * @return string la vue
     */
    public function pageConnexion($erreur = "")
    {
        $vue = new VueUser(VueUser::AUTH_USER, array('error' => $erreur));
        return $vue->render();
    }

    /**
     * methode appellee apres la validation du formulaire de connexion, fait les verifications necessaires
     *
     * @return string l erreur eventuelle
     */
    public function connexionUser()
    {
        if (isset($_POST['connexion_inc']) && $_POST['connexion_inc'] == 'connexion_f1') {

            if (!isset($_SESSION['auth'])) {
                $mailOK = false;
                if (isset($_POST['mail'])) {

                    $mailUser = htmlspecialchars($_POST['mail']);

                    if (filter_var($mailUser, FILTER_VALIDATE_EMAIL)) {
                        $mailOK = true;

                    } else {
                        return "l'adresse email est invalide";
                    }

                }

                $mdpOK = false;
                $mdpUser = "";
                if (isset($_POST['mdp'])) {
                    $mdpUser = htmlspecialchars($_POST['mdp']);
                    $mdpOK = true;
                }

                if ($mailOK && $mdpOK) {
                    $user = User::where('mailUser', '=', $mailUser)->first();
                    $hash = $user->mdpUser;
                    $salt = $user->salt;

                    if ($user != null) {
                        if (password_verify($mdpUser . $salt, $hash)) {
                            $_SESSION['auth'] = $user->idUser;
                            $app = Slim::getInstance();
                            $app->redirect($app->urlFor("connexion"));
                        } else {
                            return "le mot de passe ou l'email n'est pas bon";
                        }
                    } else {
                        return "le mot de passe ou l'email n'est pas bon";
                    }
                }
            } else {
                return "Avant de vous reconnecter, vous devez d'abord vous déconnecter";
            }

        }
    }

    /**
     * methode retournant la vue avec le formulaire de connexion
     *
     * @param string $erreur l erreur eventuelle a afficher
     * @return string la vue
     */
    public function createUser($erreur = "")
    {
        $vue = new VueUser(VueUser::CREATE_USER, array('error' => $erreur));
        return $vue->render();
    }

    /**
     * methode appellee apres validation du formulaire d inscription, permettant a un utilisateur de s inscrire
     *
     * @return string l erreur eventuelle
     */
    public function addUser()
    {
        if (isset($_POST['inscription_inc']) && $_POST['inscription_inc'] == 'inscription_f1') {
            $app = Slim::getInstance();
            $user = new User();

            $nomOK = false;
            if (isset($_POST['nom'])) {
                $nomUser = htmlspecialchars($_POST['nom']);
                $user->nomUser = $nomUser;

                $nomOK = true;
            }


            $prenomOK = false;
            if (isset($_POST['prenom'])) {
                $prenomUser = htmlspecialchars($_POST['prenom']);
                $user->prenomUser = $prenomUser;

                $prenomOK = true;
            }

            $mailOK = false;
            if (isset($_POST['mail']) && isset($_POST['mail_confirm'])) {

                $mailUser = htmlspecialchars($_POST['mail']);
                $mail_confirm = htmlspecialchars($_POST['mail_confirm']);
                if ($mailUser === $mail_confirm) {

                    if (filter_var($mailUser, FILTER_VALIDATE_EMAIL)) {

                        $userExistant = User::where('mailUser', '=', $mailUser)->first();

                        if ($userExistant === null) {
                            $user->mailUser = $mailUser;
                            $mailOK = true;
                        }

                    } else {
                        return "l'adresse email est invalide";
                    }

                } else {
                    return "les adresses email fournies sont différentes";
                }
            }

            $mdpOK = false;
            if (isset($_POST['mdp']) && isset($_POST['mdp_confirm'])) {
                $mdpUser = htmlspecialchars($_POST['mdp']);
                $mdp_confirm = htmlspecialchars($_POST['mdp_confirm']);

                if ($mdpUser === $mdp_confirm) {
                    $salt = uniqid(mt_rand(), true);

                    $hash = password_hash($mdpUser . $salt, PASSWORD_DEFAULT);
                    $user->mdpUser = $hash;
                    $user->salt = $salt;

                    $mdpOK = true;
                } else {
                    return "les mots de passe fournis sont différents";
                }
            }

            if ($nomOK && $prenomOK && $mailOK && $mdpOK) {
                $user->save();

                $app->redirect($app->urlFor("connexion"));
            }

        }
        $app->redirect($app->urlFor('index'));
    }

    /**
     * methode retournant la vue permettant de modifier le compte de l utilisateur connecte
     *
     * @param string $erreur l erreur a afficher
     * @return string la vue
     */
    public function modificationUser($erreur = "")
    {
        if (isset($_SESSION['auth'])) {

            $user = User::where('idUser', '=', $_SESSION['auth'])->first();
            $nom = $user->nomUser;
            $prenom = $user->prenomUser;

            $vue = new VueUser(VueUser::MODIFIER_USER, array($nom, $prenom, 'erreur' => $erreur));
            $res = $vue->render();
        } else {
            return "Veuillez d'abord vous connecter avant de modifier votre compte";
        }
        return $res;
    }

    /**
     * methode appellee apres validation du formulaire de modification, permettant de modifier les informations d un utilisateur
     *
     * @return string l erreur eventuelle
     */
    public function modifierUser()
    {
        if (isset($_SESSION['auth']) && isset($_POST['modification_inc']) && $_POST['modification_inc'] == 'modification_f1') {

            $user = User::where('idUser', '=', $_SESSION['auth'])->first();

            $nomOK = false;
            if (isset($_POST['nom'])) {
                $nomUser = htmlspecialchars($_POST['nom']);
                $user->nomUser = $nomUser;
                $nomOK = true;
            }


            $prenomOK = false;
            if (isset($_POST['prenom'])) {
                $prenomUser = htmlspecialchars($_POST['prenom']);
                $user->prenomUser = $prenomUser;
                $prenomOK = true;
            }

            $mdpOK = false;
            if (isset($_POST['oldmdp']) && isset($_POST['mdp']) && isset($_POST['mdp_confirm'])) {
                $oldmdp = htmlspecialchars($_POST['oldmdp']);

                $salt = $user->salt;

                if (password_verify($oldmdp . $salt, $user->mdpUser)) {

                    $mdpUser = htmlspecialchars($_POST['mdp']);
                    $mdp_confirm = htmlspecialchars($_POST['mdp_confirm']);

                    if ($mdpUser === $mdp_confirm) {
                        $salt = uniqid(mt_rand(), true);

                        $hash = password_hash($mdpUser . $salt, PASSWORD_DEFAULT);
                        $user->mdpUser = $hash;
                        $user->salt = $salt;

                        $mdpOK = true;
                    } else {
                        return "les mots de passe fournis sont différents";
                    }
                }

            }

            if ($nomOK || $prenomOK || $mdpOK) {
                $user->save();
                if ($mdpOK) {
                    $this->deconnexion();
                }
            }

        }
    }

    /**
     * methode deconnectant un utilisateur deja connecte
     *
     * @return string l erreur eventuelle
     */
    public function deconnexion()
    {
        if (isset($_SESSION['auth'])) {

            unset($_SESSION['auth']);
            session_destroy();
            $app = Slim::getInstance();
            $app->redirect($app->urlFor("index"));
        } else {
            return "Veuillez d'abord vous connecter avant de vous déconnecter";
        }
    }

}