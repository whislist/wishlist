<?php
/**
 * Created by PhpStorm.
 * User: wishlist
 * Date: 23/12/2018
 * Time: 15:16
 */

namespace wishlist\controleurs;


use Slim\Slim;
use wishlist\modele\Item;
use wishlist\modele\Liste;
use wishlist\views\VueAdministration;

class ControleurAdministration
{


    /**
     * cette methode affiche les listes d un utilisateur, vaec une vue d administration pour gerer les listes
     *
     * @param $token string le token de createur de l utilisateur
     * @return string la vue d administration
     */
    public function affichage_liste_admin($token)
    {
        $liste = Liste::where('tokenAdmin', '=', $token)->first();
        $app = Slim::getInstance();
        if ($liste) {
            $expiration = Liste::select('expiration')->where('tokenAdmin', '=', $token)->get()->implode('expiration', ', ');
            if (isset($_SESSION['auth'])) {
                $auth = $_SESSION['auth'];
            } else {
                $auth = '';
            }
            $liste = Liste::where('tokenAdmin', '=', $token)->first();
            $listeDitems = [];
            if ($liste) {
                $listeDitems = $liste->items()->get()->toArray();
            }

            $vue = new VueAdministration($listeDitems, VueAdministration::LIST_ITEM, array('token' => $token, 'etat' => $liste->etat, 'expiration' => $expiration, 'tokenPart' => $liste->tokenParticipant, 'user_id' => $liste->user_id, 'auth' => $auth));
            return $vue->render();
        } else {
            $app->redirect($app->urlFor('index'));
        }

    }

    /**
     * methode permettant au createur de supprimer un item
     */
    public function supprItem()
    {
        if (isset($_POST['supprimer'])) {
            $idItem = explode("_", $_POST['supprimer'])[1];

            $item = Item::where(['id' => $idItem])->first();

            if ($item['statutReservation'] == false || $item['cagnotte'] == false) {
                $item->delete();
            } else {
                //errreur car l'item est reserve
            }

        }

    }

    /**
     * methode permettant au createur de supprimer l image de l item
     */
    public function supprImg()
    {
        if (isset($_POST['supprimerImg'])) {
            $idItem = explode("_", $_POST['supprimerImg'])[1];

            $item = Item::where(['id' => $idItem]);
            $itemCheck = $item->first();

            if ($itemCheck['statutReservation'] == false) {
                $item->update(['img' => null]);
            } else {
                //errreur car l'item est reserve
            }

        }

    }

    /**
     * methode permettant de valider la liste
     *
     * @param $token string le token du createur de la liste
     */
    public function validerListe($token)
    {
        if (isset($_POST['valider'])) {
            $liste = Liste::where('tokenAdmin', '=', $token);
            $liste->update(array('etat' => 'F'));
        }

    }
}