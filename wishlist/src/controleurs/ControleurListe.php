<?php
/**
 * Created by PhpStorm.
 * User: palauclement
 * Date: 12/12/2018
 * Time: 17:35
 */

namespace wishlist\controleurs;

use Slim\Slim;
use wishlist\modele\Item;
use wishlist\modele\Liste;
use wishlist\views\VueListe;

class ControleurListe
{

    /**
     * methode retournant la vue pour creer une liste
     *
     * @return string la vue
     */
    public function createList()
    {

        $vue = new VueListe(VueListe::CREATE_LIST);

        return $vue->render();
    }

    /**
     * methode appelle apres validation du formulaire de creation d une liste, permettant de la creer
     */
    public function ajoutListe()
    {
        if (isset($_POST['valider_inc']) && $_POST['valider_inc'] == 'valid_f1') {

            $liste = new Liste();


            if (isset($_POST['nomCreateur'])) {
                $nomCreateur = $_POST['nomCreateur'];
                $nomCreateur = filter_var($nomCreateur, FILTER_SANITIZE_STRING);
                $liste->nomCreateur = $nomCreateur;
            }

            if (isset($_POST['titre'])) {
                $titre = ($_POST['titre']);
                $titre = filter_var($titre, FILTER_SANITIZE_STRING);
                $liste->titre = $titre;
            }

            if (isset($_POST['description'])) {
                $description = $_POST['description'];
                $description = filter_var($description, FILTER_SANITIZE_STRING);
                $liste->description = $description;
            }

            if (isset($_POST['expiration'])) {
                $expiration = $_POST['expiration'];
                $expiration = filter_var($expiration, FILTER_SANITIZE_NUMBER_FLOAT);
                $liste->expiration = $expiration;
            }


            if (isset($_POST['privacy'])) {
                $privacy = $_POST['privacy'];
                if ($privacy === "prive") {
                    $liste->privacy = 'X';
                }
            }

            if (isset($_SESSION['auth'])) {
                $liste->user_id = $_SESSION['auth'];
            }

            $liste->etat = 'C';

            $tokenAdmin = openssl_random_pseudo_bytes(32);
            $tokenAdmin = bin2hex($tokenAdmin);
            $nbtoken = Liste::where('tokenAdmin', '=', $tokenAdmin)->get();
            $count = $nbtoken->count();
            while ($count > 0) {
                $tokenAdmin = openssl_random_pseudo_bytes(32);
                $tokenAdmin = bin2hex($tokenAdmin);
                $nbtoken = Liste::where('tokenAdmin', '=', $tokenAdmin)->get();
                $count = $nbtoken->count();
            }
            $liste->tokenAdmin = $tokenAdmin;


            $tokenParticipant = openssl_random_pseudo_bytes(32);
            $tokenParticipant = bin2hex($tokenParticipant);
            $nbtoken = Liste::where('tokenParticipant', '=', $tokenParticipant)->get();
            $count = $nbtoken->count();
            while ($count > 0) {
                $tokenParticipant = openssl_random_pseudo_bytes(32);
                $tokenParticipant = bin2hex($tokenParticipant);
                $nbtoken = Liste::where('tokenParticipant', '=', $tokenParticipant)->get();
                $count = $nbtoken->count();
            }
            $liste->tokenParticipant = $tokenParticipant;

            $app = Slim::getInstance();
            $cookie = $app->getCookie('MyList');
            $newCookie = $tokenAdmin;
            $cookie .= "||" . $newCookie;
            $app->setCookie('MyList', $cookie, '+1 year');
            $liste->save();
            $app->redirect($app->urlFor('listeAdm', array('token' => $tokenAdmin)));

        }
    }

    /**
     * methode retournant le formulaire de creation d un item
     *
     * @param string $erreur l erreur a afficher
     * @return string la vue
     */
    public function createItem($erreur = "")
    {

        $vue = new VueListe(VueListe::AJOUT_ITEM, array('error' => $erreur));

        return $vue->render();
    }

    /**
     * methode appellee apres validation du formulaire d ajout d un item, permettant de l ajouter a une liste
     *
     * @param $token string le token de createur de la liste
     */
    public function ajoutItem($token)
    {
        if (isset($_POST['valider_inc']) && ($_POST['valider_inc'] == 'valid_f1' || $_POST['valider_inc'] == 'valid_f2')) {

            $item = new Item();
            $liste = Liste::where('tokenAdmin', '=', $token)->first();
            $item->liste_id = $liste->no;

            if (isset($_POST['nom'])) {
                $nom = $_POST['nom'];
                $nom = filter_var($nom, FILTER_SANITIZE_STRING);
                $item->nom = $nom;

            }

            if (isset($_POST['descr'])) {
                $descr = $_POST['descr'];
                $descr = filter_var($descr, FILTER_SANITIZE_STRING);
                $item->descr = $descr;
            }

            if (isset($_POST['tarif'])) {
                $tarif = $_POST['tarif'];
                $tarif = str_replace(",", ".", $tarif);
                $tarif = filter_var($tarif, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                $item->tarif = $tarif;
            }

            if (isset($_POST['url'])) {
                $url = $_POST['url'];
                if (!empty($url) && !filter_var($url, FILTER_VALIDATE_URL)) {
                    return "url invalide";
                } else {

                    $item->url = $url;
                }
            }


            if (isset($_POST['cagnotte'])) {
                $item->cagnotte = true;

            }
            $choix = 0;
            if (isset($_POST['choix'])) {
                if ($_POST['choix'] == "bfileToUpload") {
                    $choix = 1;
                }
                if ($_POST['choix'] == "burlImg") {
                    $choix = 2;
                }
                if ($_POST['choix'] == "bnomImg") {
                    $choix = 3;
                }

            }


            if (isset($_POST['urlImg']) && $choix == 2) {
                $item->urlImg = true;
                $urlImg = $_POST['urlImg'];
                if (!empty($urlImg) && !filter_var($urlImg, FILTER_VALIDATE_URL)) {
                    return "url invalide";
                } else {

                    $item->img = $urlImg;
                }
            }
            if (isset($_POST['nomImg']) && $choix == 3) {
                $item->urlImg = false;
                $nomImg = $_POST['nomImg'];
                $nomImg = filter_var($nomImg, FILTER_SANITIZE_STRING);
                $item->img = $nomImg;
            }

            if (isset($_FILES["fileToUpload"]["name"]) && $choix == 1) {
                $chemin = "img/" . basename($_FILES["fileToUpload"]["name"]);
                $uploadOk = 1;
                $imageFileType = strtolower(pathinfo($chemin, PATHINFO_EXTENSION));


                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                if ($check !== false) {
                    $uploadOk = 1;
                } else {
                    return "Ce n'est pas une image";
                }
                $i = 1;
                $cheminDiv = explode(".", $chemin);
                while (file_exists($chemin)) {
                    $cheminDivIte = explode(" (" . ($i - 1) . ")", $cheminDiv[0]);
                    if (!empty($cheminDivIte)) {
                        $cheminDiv[0] = $cheminDivIte[0] . " (" . $i . ")";
                    } else {
                        $cheminDiv[0] .= " (" . $i . ")";
                    }
                    $i++;
                    $chemin = $cheminDiv[0] . "." . $cheminDiv[1];
                }

                if ($_FILES["fileToUpload"]["size"] > 1000000) {
                    return "Fichier trop lourd";
                }

                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                    return "PAs bon format de fichier";
                }

                if ($uploadOk == 0) {
                    return "Pas possible d'upload.";

                } else {
                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $chemin)) {
                        $item->img = basename($_FILES["fileToUpload"]["name"]);
                    } else {
                        return "Erreur";
                    }
                }
            }


            $item->statutReservation = FALSE;

            $item->save();
            $app = Slim::getInstance();
            if ($_POST['valider_inc'] == 'valid_f2') {
                $app->redirect($app->urlFor('listeAdm', array('token' => $token)));
            } else {
                $app->redirect($app->urlFor('liste/ajout', array('token' => $token)));
            }


        }
    }

    /**
     * methode affichant le formulaire permettant de modifier un item
     *
     * @param $token string le token du createur de la liste qui contient l item
     * @param $id int l id de la liste qui contient l item
     * @param string $erreur l erreur a afficher
     * @return string la vue
     */
    public function modifItem($token, $id, $erreur = "")
    {

        $num = Liste::select('no')->where('tokenAdmin', '=', $token)->get()->implode('no', ', ');
        $item = Item::where('id', '=', $id)->where('liste_id', '=', $num);
        $nomItem = $item->select('nom')->get()->implode('nom', ', ');
        $description = $item->select('descr')->get()->implode('descr', ', ');
        $tarif = $item->select('tarif')->get()->implode('tarif', ', ');
        $url = $item->select('url')->get()->implode('url', ', ');


        $vue = new VueListe(VueListe::MODIF_ITEM, array('nom' => $nomItem, 'descr' => $description, 'tarif' => $tarif, 'url' => $url, 'error' => $erreur));

        return $vue->render();
    }

    /**
     * methode appelle apres validation du formulaire de modification d un item pour modifier l item d une liste
     *
     * @param $token string le token de createur de la liste qui contient l item
     * @param $id int l id de la liste
     */
    public function modificationItem($token, $id)
    {
        if (isset($_POST['valider_inc']) && ($_POST['valider_inc'] == 'valid_f1' || $_POST['valider_inc'] == 'valid_f2')) {

            $num = Liste::select('no')->where('tokenAdmin', '=', $token)->get()->implode('no', ', ');
            $item = Item::where('id', '=', $id);
            $num_liste = $item->select('liste_id')->get()->implode('liste_id', ', ');
            $reserver = $item->select('statutReservation')->get()->implode('statutReservation', ', ');

            if ($num === $num_liste && $reserver == FALSE) {

                if (isset($_POST['nom'])) {
                    $nom = $_POST['nom'];
                    $nom = filter_var($nom, FILTER_SANITIZE_STRING);
                    $item->update(['nom' => $nom]);
                }

                if (isset($_POST['descr'])) {
                    $descr = $_POST['descr'];
                    $descr = filter_var($descr, FILTER_SANITIZE_STRING);
                    $item->update(['descr' => $descr]);
                }

                if (isset($_POST['tarif'])) {
                    $tarif = $_POST['tarif'];
                    $tarif = str_replace(",", ".", $tarif);
                    $tarif = filter_var($tarif, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $item->update(['tarif' => $tarif]);

                }

                if (isset($_POST['url'])) {
                    $url = $_POST['url'];
                    if (!empty($url) && !filter_var($url, FILTER_VALIDATE_URL)) {
                        return "url invalide";
                    } else {
                        $item->update(['url' => $url]);
                    }
                }


                $choix = 0;
                if (isset($_POST['choix'])) {
                    if ($_POST['choix'] == "bfileToUpload") {
                        $choix = 1;
                    }
                    if ($_POST['choix'] == "burlImg") {
                        $choix = 2;
                    }
                    if ($_POST['choix'] == "bnomImg") {
                        $choix = 3;
                    }

                }


                if (isset($_POST['urlImg']) && $choix == 2) {
                    $item->update(['urlImg' => true]);
                    $urlImg = $_POST['urlImg'];
                    if (!filter_var($urlImg, FILTER_VALIDATE_URL)) {
                        return "url invalide";

                    } else {
                        $item->update(['img' => $urlImg]);
                    }
                }

                if (isset($_POST['nomImg']) && $choix == 3) {
                    $item->update(['urlImg' => false]);
                    $nomImg = $_POST['nomImg'];
                    $nomImg = filter_var($nomImg, FILTER_SANITIZE_STRING);
                    $item->update(['img' => $nomImg]);


                }

                if (isset($_FILES["fileToUpload"]["name"]) && $choix == 1) {
                    $item->update(['urlImg' => false]);
                    $chemin = "img/" . basename($_FILES["fileToUpload"]["name"]);
                    $uploadOk = 1;
                    $imageFileType = strtolower(pathinfo($chemin, PATHINFO_EXTENSION));


                    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                    if ($check !== false) {

                        $uploadOk = 1;
                    } else {
                        return "Ce n'est pas une image";
                        $uploadOk = 0;
                    }
                    $i = 1;
                    $cheminDiv = explode(".", $chemin);
                    while (file_exists($chemin)) {
                        $cheminDiv[0] .= " (" . $i . ")";
                        $i++;
                        $chemin = $cheminDiv[0] . $cheminDiv[1];
                    }

                    if ($_FILES["fileToUpload"]["size"] > 1000000) {
                        return "Fichier trop volumineux";
                        $uploadOk = 0;
                    }

                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                        return "Pas bon format d'image";
                        $uploadOk = 0;
                    }

                    if ($uploadOk == 0) {
                        return "Pas possible d'upload.";

                    } else {
                        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $chemin)) {
                            $img = basename($_FILES["fileToUpload"]["name"]);
                            $item->update(['img' => $img]);
                        } else {
                            return "Erreur";
                        }
                    }
                }


                $app = Slim::getInstance();
                if ($_POST['valider_inc'] == 'valid_f2') {
                    $app->redirect($app->urlFor('listeAdm', array('token' => $token)));
                } else {
                    $app->redirect($app->urlFor('liste/ajout', array('token' => $token)));
                }

            }
        }
    }

    /**
     * methode retournant le formulaire de modification d une liste
     *
     * @param $token string le token du createur de la liste
     * @return string la vue
     */
    public function modifListe($token)
    {

        $liste = Liste::where('tokenAdmin', '=', $token);
        $titre = $liste->select('titre')->get()->implode('titre', ', ');
        $nomCreateur = $liste->select('nomCreateur')->get()->implode('nomCreateur', ', ');
        $description = $liste->select('description')->get()->implode('description', ', ');
        $expiration = $liste->select('expiration')->get()->implode('expiration', ', ');


        $vue = new VueListe(VueListe::MODIF_LIST, array('titre' => $titre, 'descr' => $description, 'nomCreateur' => $nomCreateur, 'expiration' => $expiration));

        return $vue->render();
    }

    /**
     * methode appellee apres validation du formulaire de modification d une liste, permettant de la modifier
     *
     * @param $token string le token du createur de la liste
     */
    public function modificationListe($token)
    {
        if (isset($_POST['valider_inc']) && ($_POST['valider_inc'] == 'valid_f1')) {

            $liste = Liste::where('tokenAdmin', '=', $token);

            if (isset($_POST['nomCreateur'])) {
                $nomCreateur = $_POST['nomCreateur'];
                $nomCreateur = filter_var($nomCreateur, FILTER_SANITIZE_STRING);
                $liste->update(['nomCreateur' => $nomCreateur]);
            }

            if (isset($_POST['titre'])) {
                $titre = $_POST['titre'];
                $titre = filter_var($titre, FILTER_SANITIZE_STRING);
                $liste->update(['titre' => $titre]);
            }

            if (isset($_POST['description'])) {
                $description = $_POST['description'];
                $description = filter_var($description, FILTER_SANITIZE_STRING);
                $liste->update(['description' => $description]);
            }

            if (isset($_POST['expiration'])) {
                $expiration = $_POST['expiration'];
                $expiration = filter_var($expiration, FILTER_SANITIZE_NUMBER_FLOAT);
                $liste->update(['expiration' => $expiration]);
            }

            if (isset($_POST['privacy'])) {
                $privacy = $_POST['privacy'];
                if ($privacy === "prive") {
                    $liste->update(['privacy' => 'X']);
                } else {
                    $liste->update(['privacy' => null]);
                }
            }

            $app = Slim::getInstance();
            if ($_POST['valider_inc'] == 'valid_f1') {
                $app->redirect($app->urlFor('listeAdm', array('token' => $liste->first()->tokenAdmin)));
            }
        }
    }

}