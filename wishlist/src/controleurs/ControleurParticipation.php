<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 11/12/2018
 * Time: 12:34
 */

namespace wishlist\controleurs;


use Slim\Slim;
use wishlist\modele\Cagnotte;
use wishlist\modele\Item;
use wishlist\modele\Liste;
use wishlist\modele\Message;
use wishlist\modele\ParticipeCagnotte;
use wishlist\views\VueParticipant;

class ControleurParticipation
{
    /**
     * le nombre de listes affichees par page
     */
    const nbParPage = 5;

    /**
     * methode permettant d afficher l item d une liste
     *
     * @param $token string le token du participant
     * @param $id int l id de la liste
     * @param string $erreur l erreur a afficher
     * @return string la vue
     */
    public function displayUnitem($token, $id, $erreur = "")
    {
        $today = date('Y-m-d');
        $liste = Liste::where('tokenParticipant', '=', $token)->where('etat', '=', 'F')->where('expiration','>',$today)->first();
        $app = Slim::getInstance();
        if ($liste) {
            $tokenAdmin = $liste->tokenAdmin;

            if (!($this->verifyToken($tokenAdmin))) {
                $idliste = $liste['no'];
                $item = Item::where('id', '=', $id)->where('liste_id', '=', $idliste)->first();
                $cagnottes = ParticipeCagnotte::where('id', '=', $id)->join("Cagnotte", 'participeCagnotte.idCagnotte', '=', 'Cagnotte.idCagnotte')->get();
                $somme = $cagnottes->sum('prix');
                $vue = new VueParticipant($item, VueParticipant::ITEM_VIEW, array('error' => $erreur,'somme' => $somme));
                return $vue->render();
            } else {
                $app->redirect($app->urlFor('listeAdm', array('token' => $tokenAdmin)));
            }
        } else {
            $app->redirect($app->urlFor('index'));
        }
    }

    /**
     * methode retournant verifiant un token
     *
     * @param $token string le token a verifier
     * @return bool vrai si
     */
    private function verifyToken($token)
    {
        $app = Slim::getInstance();
        $cookieListe = $app->getCookie('MyList');
        $cookieListe = explode('||', $cookieListe);
        return in_array($token, $cookieListe);
    }

    /**
     * cette methode permet d afficher les listes d un createur quand il est connecte
     */
    public function displayMesListes()
    {
        if(isset($_SESSION['auth'])){
            $page = 1;
            if (isset($_GET['page']) and intval($_GET['page'])) {
                $page = $_GET['page'];
            }

            $liste = Liste::where('user_id', '=', $_SESSION['auth'])->where('etat', '=', 'F');
            $listeCount = $liste->count();

            $liste = Liste::where('user_id', '=', $_SESSION['auth'])->where('etat', '=', 'F')->skip(ControleurParticipation::nbParPage * $page - ControleurParticipation::nbParPage)->take(ControleurParticipation::nbParPage)->get()->toArray();

            $vue = new VueParticipant($liste, VueParticipant::LIST_LIST, array('count' => $listeCount));
            return $vue->render();
        }

    }

    /**
     * methode affichant les items d une liste
     *
     * @param $token string le token du participant
     * @return string la vue
     */
    public function displayItemsDuneListe($token)
    {
        $today = date('Y-m-d');
        $liste = Liste::where('tokenParticipant', '=', $token)->where('expiration','>',$today)->first();
        $app = Slim::getInstance();
        if ($liste) {
            $tokenAdmin = $liste->tokenAdmin;

            if (!($this->verifyToken($tokenAdmin))) {
                $listeDitems = $liste->items()->get()->toArray();
                $listeMessage = $liste->messages()->get()->toArray();
                $vue = new VueParticipant($listeDitems, VueParticipant::LIST_ITEM, array('token' => $token, 'message' => $listeMessage));
                return $vue->render();
            } else {
                $app->redirect($app->urlFor('listeAdm', array('token' => $tokenAdmin)));
            }
        } else {
            $app->redirect($app->urlFor('index'));
        }
    }

    /**
     * methode affichant toutes les listes publiques
     *
     * @return string la vue
     */
    public function displayAllListe()
    {
        $page = 1;
        if (isset($_GET['page']) and intval($_GET['page'])) {
            $page = $_GET['page'];

        }
        $recherche = '';
        if (isset($_GET['recherche'])) {
            $recherche = $_GET['recherche'];

        }
        $today = date('Y-m-d');
        $listes = Liste::where('titre', 'LIKE', '%' . $recherche . '%')->where('expiration','>',$today)->whereNull('privacy')->where('etat', '=', 'F')->get();
        $listesCount = $listes->count();
        $listeArray = Liste::where('titre', 'LIKE', '%' . $recherche . '%')->where('expiration','>',$today)->whereNull('privacy')->where('etat', '=', 'F')->skip(ControleurParticipation::nbParPage * $page - ControleurParticipation::nbParPage)->take(ControleurParticipation::nbParPage)->get()->toArray();
        $vue = new VueParticipant($listeArray, VueParticipant::LIST_LIST, array('count' => $listesCount));
        return $vue->render();
    }

    /**
     * methode appellee apres validation du formulaire de reservation, permettant a un utilisateur de reserver un item
     *
     * @param $token string le token du participant
     * @param $id int l id de l item
     */
    public function reservationItem($token, $id)
    {

        if (isset($_POST['reserv_inc']) && $_POST['reserv_inc'] == 'reserv_f1') {
            $item = Item::where('id', '=', $id);
            $itemCag = Item::where('id', '=', $id)->first();


            if (isset($_POST['nom'])) {
                $nomReservation = $_POST['nom'];
                if (!filter_var($nomReservation, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/[A-Za-z\s]+/")))) {
                    echo 'Bad name';
                } else {

                    $item->update(['nomReservation' => $nomReservation]);

                }
                // $item->nomReservation = $nomReservation;
            }

            if (isset($_POST['prenom'])) {
                $prenomReservation = htmlspecialchars($_POST['prenom']);
                $item->update(['prenomReservation' => $prenomReservation]);

                //$item->prenomReservation = $prenomReservation;
            }

            if (isset($_POST['message'])) {
                $messageReservation = htmlspecialchars($_POST['message']);
                $item->update(['message' => $messageReservation]);

            }
            $presentCagnotte = false;
            if (isset($_POST['prix_participe'])) {
                $prix_participe = $_POST['prix_participe'];

                $cagnottes = ParticipeCagnotte::where('id', '=', $id)->join("Cagnotte", 'participeCagnotte.idCagnotte', '=', 'Cagnotte.idCagnotte')->get();
                $somme = $cagnottes->sum('prix');

                if ($prix_participe > ($itemCag->tarif - $somme)) {
                    return $itemCag->tarif - $somme;
                } else {
                    foreach ($cagnottes as $c) {
                        if ($c->nomReservation === $nomReservation && $c->prenomReservation === $prenomReservation) {

                            $presentCagnotte = true;

                        }
                    }

                }
            }
            if ($itemCag->cagnotte == true) {

                if ($presentCagnotte == false) {
                    $cagnotte = new Cagnotte();
                    $cagnotte->nomReservation = $nomReservation;
                    $cagnotte->prenomReservation = $prenomReservation;
                    $cagnotte->prix = $prix_participe;
                    $cagnotte->save();
                    $par = new ParticipeCagnotte();
                    $par->idCagnotte = $cagnotte->idCagnotte;
                    $par->id = $id;
                    $par->save();
                }
                if ($presentCagnotte == true) {
                    $cagnotte = Cagnotte::where('nomReservation', '=', $nomReservation)->where('prenomReservation', '=', $prenomReservation)->first();

                    $tot = $cagnotte->prix + $prix_participe;
                    $cagnotte->prix = $tot;
                    $cagnotte->save();
                }


            }


        }
        setcookie("nomReservation", $_POST['nom'], time() + 60 * 60 * 24 * 30);
        setcookie("prenomReservation", $_POST['prenom'], time() + 60 * 60 * 24 * 30);


    }

    /**
     * methode permettant d enregistrer le message rentre par l utilisateur
     *
     * @param $token string le token de participation
     */
    public function ajouteMessage($token)
    {
        $liste = Liste::where('tokenParticipant', '=', $token)->first();

        $message = new Message();
        if (isset($_POST['nom'])) {
            $nomReservation = $_POST['nom'];
            if (!filter_var($nomReservation, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/[A-Za-z\s]+/")))) {
                echo 'Bad name';
            } else {
                $message->nom = $nomReservation;
            }
            // $item->nomReservation = $nomReservation;
        }

        if (isset($_POST['prenom'])) {
            $prenomReservation = htmlspecialchars($_POST['prenom']);
            $message->prenom = $prenomReservation;
            //$item->prenomReservation = $prenomReservation;
        }

        if (isset($_POST['message'])) {
            $messageReservation = htmlspecialchars($_POST['message']);
            $message->message = $messageReservation;
        }

        $message->id_liste = $liste->no;
        $message->save();
        $app = Slim::getInstance();
        $app->redirect($app->urlFor('liste', array('token' => $token)));

    }
}