<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 05/12/2018
 * Time: 14:23
 */

namespace wishlist\controleurs;


use wishlist\views\VueIndex;

class DisplayPrincipal
{
    /**
     * la methode affichant la page principale
     *
     * @return string la vue
     */
    public function principal()
    {
        $vue = new VueIndex(VueIndex::INDEX);
        return $vue->render();

    }
}