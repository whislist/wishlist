<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 19/11/2018
 * Time: 18:23
 */

namespace test;

use wishlist\modele\Item;
use wishlist\modele\Liste;

class test{

    static function getListe($id){
        $liste = Liste::select('titre')->where('no','=',$id)->first();
        if(isset($liste)) {
            return $liste->titre;
        }
        else{
            return 404;
        }
    }

    static function listeSouhait(){
        $listes = Liste::select('titre','no')->get();
        foreach ($listes as $valeur){
            $items = Item::where('liste_id','=',$valeur['no'])->get();
            echo $valeur['titre'] . ":" . "\n";
            foreach ($items as $item) {
                echo "    ".$item['nom'] . " ".$item['desc'] . "\n";
            }
        }

    }


    static function listeItem(){
        $listes = Item::select('nom')->get();
        foreach ($listes as $valeur) {
            echo $valeur . "\n";
        }

    }

    static function itemId($id){
        $listes = Item::select('nom')->where('id','=',$id)->get();
        foreach ($listes as $valeur) {
            echo $valeur . "\n";
        }
    }

    static function addItemListe($nom,$idListe){
        $item = new Item();
        $item->nom = $nom;
        $item->liste_id = $idListe;
        $item->save();
    }

    /*
     * lister les items d'une liste donnée dont l'id est passé en paramètre.
     */
    static function listItem2($idliste){
        $liste = Liste::where('no','=',$idliste)->first();
        $items = $liste->items()->get();
        foreach ($items as $valeur){
            echo $valeur . "\n";
        }
    }

    /*
     * indiquer le nom de la liste de souhait dans la liste des items
     */

    static function avoirListe($id){
        $item = Item::where('id','=',$id)->first();
        $liste = $item->liste()->first();
        echo $liste->description . "\n";
    }
}