<?php
/**
 * File:  index.php
 * Creation Date: 04/12/2017
 * description:
 *
 * @author: canals
 */

use wishlist\Connexion;
use test\test;

require_once __DIR__ . '/vendor/autoload.php';

session_start();

Connexion::setConfig('src/conf/conf.ini');
Connexion::makeConnection();

$app = new \Slim\Slim(array(
    'cookies.encrypt' => true,
    'cookies.secret_key' => 'my_secret_key',
    'cookies.cipher' => MCRYPT_RIJNDAEL_256,
    'cookies.cipher_mode' => MCRYPT_MODE_CBC));


$app->get('/', function() use ($app) {
    $display = new \wishlist\controleurs\DisplayPrincipal();
    echo $display->principal();
})->name('index');


$app->get('/liste/participation/:token', function ($token) use ($app) {
    $controleur = new \wishlist\controleurs\ControleurParticipation();
    echo $controleur->displayItemsDuneListe($token);
})->name('liste')->conditions(array('token' => '[0-9a-fA-F]+'));


$app->post('/liste/participation/:token' ,function ($token){
    $controleur = new \wishlist\controleurs\ControleurParticipation();
    $controleur->ajouteMessage($token);
});

$app->get('/liste', function () use ($app) {
    $controleur = new \wishlist\controleurs\ControleurParticipation();
    echo $controleur->displayAllListe();
})->name('listeItem');

$app->get('/mesListes', function () use ($app) {
    $controleur = new \wishlist\controleurs\ControleurParticipation();
    echo $controleur->displayMesListes();
})->name('mesListes');

$app->get('/liste/participation/:token/:id', function ($token,$id){
    $controleur = new \wishlist\controleurs\ControleurParticipation();
    echo $controleur->displayUnitem($token,$id);
})->name('item')->conditions(array('token' => '[0-9a-fA-F]+','id' => '[0-9]+'));

$app->post('/liste/participation/:token/:id', function ($token,$id){
    $controleur = new \wishlist\controleurs\ControleurParticipation();
    $erreur = $controleur->reservationItem($token,$id);
    echo $controleur->displayUnitem($token,$id,$erreur);
});
$app->get('/liste/administrateur/:token', function ($token) use ($app) {
    $controleur = new \wishlist\controleurs\ControleurAdministration();
    echo $controleur->affichage_liste_admin($token);
})->name('listeAdm')->conditions(array('token' => '[0-9a-fA-F]+'));

$app->post('/liste/administrateur/:token', function ($token) use ($app) {
    $controleur = new \wishlist\controleurs\ControleurAdministration();
    $controleur->supprItem();
    $controleur->supprImg();
    $controleur->validerListe($token);
    echo $controleur->affichage_liste_admin($token);
})->conditions(array('token' => '[0-9a-fA-F]+','id' => '[0-9]+'));


$app->get('/liste/administrateur/creer', function (){
    $controleur = new \wishlist\controleurs\ControleurListe();
    echo $controleur->createList();
})->name('listcreer');

$app->post('/liste/administrateur/creer', function (){
    $controleur = new \wishlist\controleurs\ControleurListe();
    $controleur->ajoutListe();
});

$app->get('/liste/administrateur/ajout/:token', function ($token){
    $controleur = new \wishlist\controleurs\ControleurListe();
    echo $controleur->createItem();
})->name('liste/ajout');

$app->post('/liste/administrateur/ajout/:token', function ($token){
    $controleur = new \wishlist\controleurs\ControleurListe();
    $erreur = $controleur->ajoutItem($token);
    echo $controleur->createItem($erreur);
});

$app->get('/liste/administrateur/modif/:token/:id', function ($token,$id){
    $controleur = new \wishlist\controleurs\ControleurListe();
    echo $controleur->modifItem($token, $id);
})->name('modifItem')->conditions(array('token' => '[0-9a-fA-F]+','id' => '[0-9]+'));

$app->post('/liste/administrateur/modif/:token/:id', function ($token, $id){
    $controleur = new \wishlist\controleurs\ControleurListe();
    $erreur = $controleur->modificationItem($token, $id);
    echo $controleur->modifItem($token, $id,$erreur);

});

$app->get('/liste/administrateur/modif/:token', function ($token){
    $controleur = new \wishlist\controleurs\ControleurListe();
    echo $controleur->modifListe($token);
})->name('modifListe')->conditions(array('token' => '[0-9a-fA-F]+'));

$app->post('/liste/administrateur/modif/:token', function ($token){
    $controleur = new \wishlist\controleurs\ControleurListe();
    $controleur->modificationListe($token);
});

$app->get('/authentification/connexion', function (){
    $controleur = new \wishlist\controleurs\ControleurAuthentification();
    echo $controleur->pageConnexion();
})->name('connexion');

$app->post('/authentification/connexion', function (){
    $controleur = new \wishlist\controleurs\ControleurAuthentification();
    $erreur = $controleur->connexionUser();
    echo $controleur->pageConnexion($erreur);
});

$app->get('/authentification/deconnexion', function (){
    $controleur = new \wishlist\controleurs\ControleurAuthentification();
    $controleur->deconnexion();
})->name('deconnexion');

$app->get('/authentification/creer', function (){
    $controleur = new \wishlist\controleurs\ControleurAuthentification();
    echo $controleur->createUser();
})->name('inscription');

$app->get('/authentification/modifier', function (){
    $controleur = new \wishlist\controleurs\ControleurAuthentification();
    echo $controleur->modificationUser();
})->name('modification');

$app->post('/authentification/modifier', function (){
    $controleur = new \wishlist\controleurs\ControleurAuthentification();
    $erreur = $controleur->modifierUser();
    echo $controleur->modificationUser($erreur);
});

$app->post('/authentification/creer', function (){
    $controleur = new \wishlist\controleurs\ControleurAuthentification();
    $erreur = $controleur->addUser();
    echo $controleur->createUser($erreur);
});

$app->error(function (\Exception $e) use ($app) {
    $app->render('error.php');
});


$app->run();


?>

